import React from 'react';
import { Drawer, Row, Button } from 'antd';
import { any, string, bool, func, number } from 'prop-types';

import styles from './styles.module.scss';
import './styles.scss';

const NodeDrawerContainer = ({
    title,
    placement,
    visible,
    onClose,
    onSave,
    content,
    width,
    closeBtn,
    saveBtn
}) => {
    return (
        <Drawer
            title={(
                <span className={styles['title']}>
                    {title}
                </span>
            )}
            placement={placement}
            closable={false}
            onClose={onClose}
            visible={visible}
            width={width}
            mask={false}
            className="node-drawer-container"
        >
            {content}
            <div className={styles['group-btn']}>
                {closeBtn && (
                    <Button
                        className={styles['close-btn']}
                        onClick={onClose}
                    >
                        Đóng
                    </Button>
                )}
                {saveBtn && (
                    <Button
                        className={styles['save-btn']}
                        onClick={onSave}
                    >
                        Lưu
                    </Button>
                )}

            </div>

        </Drawer>
    )
};

NodeDrawerContainer.defaultProps = {
    placement: 'right',
    width: 350,
    saveBtn: true,
    closeBtn: true
};

NodeDrawerContainer.propTypes = {
    title: any,
    placement: string,
    visible: bool,
    onClose: func,
    onSave: func,
    helpLink: string,
    content: any,
    width: number,
    closeBtn: bool,
    saveBtn: bool
};

export default NodeDrawerContainer;
