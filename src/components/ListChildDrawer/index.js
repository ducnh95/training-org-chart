import React, { useState, useEffect } from 'react';
import InfoIcon from '@material-ui/icons/Info';
import { Input, Icon } from 'antd';
import { connect } from 'react-redux';
import { isEmpty, orderBy } from 'lodash';
import withScrolling from 'react-dnd-scrollzone';

import {
    toggleListChildDrawer,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga
} from '../../module/actions/orgChart';
import {
    selectShowListChildDrawer,
    selectListChildDrawerData,
} from '../../module/selectors/orgChart';
import NodeDrawerContainer from '../NodeDrawerContainer';
import styles from './styles.module.scss';
import { TYPE_ROLE, TYPE_DEPARTMENT, TYPE_ASSISTANT } from '../../variables/orgChart';
import ListChildWithoutDepartment from '../ListChildDiv/ListChildWithoutDepartment';
import ListChildWithDepartment from '../ListChildDiv/ListChildWithDepartment';

const getTitle = (nodeData = {}) => {
    const { type } = nodeData;
    let title = '';
    if (type === TYPE_ROLE) {
        const name = nodeData.name;
        title = `Danh sách cấp dưới của ${name}`;
    }

    return title;
}

const ListChildDrawer = ({
    showListChildDrawer,
    toggleListChildDrawer,
    listChildDrawerData,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga
}) => {
    const [title, setTitle] = useState(getTitle(listChildDrawerData));
    const [listNodeWithoutDepartment, setListNodeWithoutDepartment] = useState([]);
    const [listDepartment, setListDepartment] = useState([]);
    const onClose = () => {
        toggleListChildDrawer({});
    };

    useEffect(() => {
        if (!isEmpty(listChildDrawerData)) {
            setTitle(getTitle(listChildDrawerData));
            const newListNodeWithoutDepartment = [];
            const newListDepartment = [];
            Object.values(listChildDrawerData.children).forEach((item) => {
                const { type } = item;
                if (type === TYPE_ROLE || type === TYPE_ASSISTANT) {
                    newListNodeWithoutDepartment.push(item);
                }
                if (type === TYPE_DEPARTMENT) {
                    newListDepartment.push(item);
                }
            })
            setListNodeWithoutDepartment(newListNodeWithoutDepartment);
            setListDepartment(newListDepartment);
        }
        
    }, [listChildDrawerData]);

    return (
        <NodeDrawerContainer
            title={title}
            visible={!!listChildDrawerData}
            // visible={true}
            onClose={onClose}
            width={450}
            saveBtn={false}
            content={(
                <ListChildDrawerContent
                    listNodeWithoutDepartment={listNodeWithoutDepartment}
                    listDepartment={listDepartment}
                    showListChildDrawer={showListChildDrawer}
                    toggleNodeDrawer={toggleNodeDrawer}
                    changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                    changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
                />
            )}
        />
    )
};

const ListChildDrawerContent = ({
    listNodeWithoutDepartment,
    listDepartment,
    showListChildDrawer,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga
}) => {
    return (
        <div className={styles['container']}>
            <div className="mb-3">
                <InfoIcon className={styles['info-icon']} />
                <span>
                    Kéo thả để thay đổi vị trí công việc trong danh sách
                </span>
            </div>
            <Input
                placeholder="Nhập thông tin cần tìm kiếm"
                prefix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
            <div className={styles['list-child-container']}>
                <ListChildWithoutDepartment
                    listNodeWithoutDepartment={listNodeWithoutDepartment}
                    toggleNodeDrawer={toggleNodeDrawer}
                    showListChildDrawer={showListChildDrawer}
                    changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                    changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
                />
                {orderBy(listDepartment, 'order', 'desc').map((department, index) => (
                    <ListChildWithDepartment
                        nodeData={department}
                        key={department.id}
                        showListChildDrawer={showListChildDrawer}
                        toggleNodeDrawer={toggleNodeDrawer}
                        isLastItem={index === (listDepartment.length - 1)}
                        changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                        changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
                    />
                ))}
            </div>
        </div>
    )
};

export default connect((state) => ({
    showListChildDrawer: selectShowListChildDrawer(state),
    listChildDrawerData: selectListChildDrawerData(state)
}), {
    toggleListChildDrawer,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga
})(ListChildDrawer);