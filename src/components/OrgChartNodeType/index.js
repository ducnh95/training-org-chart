import React from 'react';
import { useDrag, DragPreviewImage } from 'react-dnd';
import { any, string } from 'prop-types';

// import dragPreview from '../../fakeData/drag-preview.svg';
import dragPreview from '../../fakeData/dragPreview';
import { ORG_CHART_NODE_TYPE } from '../../variables/orgChart';
import styles from './styles.module.scss';
import avatar from '../../fakeData/avatar';

const OrgChartNodeType = ({ icon, label, nodeType }) => {
    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: ORG_CHART_NODE_TYPE,
            nodeType
        },
        end: (item, monitor) => {
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    })

    const opacity = isDragging ? 0.4 : 1;

    return (
        <>
        <DragPreviewImage connect={preview} src={dragPreview} />
        <div className={styles['container']} style={{ opacity }} ref={drag}>
            <div className={styles['icon']}>
                {icon}
            </div>
            <div className={styles['label']}>
                {label}
            </div>
        </div>
        </>
    )
};

OrgChartNodeType.propTypes = {
    icon: any,
    label: any,
    nodeType: string
};

export default OrgChartNodeType;
