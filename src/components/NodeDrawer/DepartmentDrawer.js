import React, { useEffect, useState } from 'react';
import { AutoComplete, Input } from 'antd';

import listDepartment from '../../fakeData/listDepartment';
import NodeDrawerContainer from '../NodeDrawerContainer';
import styles from './styles.module.scss';
import { ACTION_EDIT_DEPARTMENT, ACTION_ADD_DEPARTMENT } from '../../variables/orgChart';

const generateDepartmentId = (name) => {
    const listWord = name.split(' ');
    if (listWord.length === 1) {
        return name.substring(0, 3).toUpperCase();
    }
    if (listWord.length === 2) {
        return `${listWord[0].substring(0,2)}${listWord[1].substring(0, 1)}`.toUpperCase(); 
    }
    return `${listWord[0].substring(0,1)}${listWord[1].substring(0, 1)}${listWord[2].substring(0, 1)}`.toUpperCase(); 
}

const DepartmentDrawer = ({
    onClose,
    nodeDrawerData,
    onSaveNodeDrawer
}) => {
    const { node, action, type } = nodeDrawerData;
    const [name, setName] = useState('');
    const [department, setDepartment] = useState('');
    const [listDepartmentSource, setListDepartmentSource] = useState([]);

    useEffect(() => {
        if (action === ACTION_EDIT_DEPARTMENT && node) {
            const { name, department } = node;
            setName(name);
            setDepartment(department);
        }
        if (action === ACTION_ADD_DEPARTMENT) {
            setName('');
            setDepartment();
        }
    },[node, action])

    useEffect(() => {
        const newSource = listDepartment.filter(item => item.name.toLowerCase().includes(name.toLowerCase()));
        setListDepartmentSource(
            newSource.map(item => ({
                value: item.id,
                text: item.name
            }))
        );
    }, [name]);

    const onChangeName = (value) => {
        setName(value);
        const departmentId = generateDepartmentId(value);
        setDepartment({
            id: departmentId,
            name: value
        })
    };

    const onSelectDepartment = (value, option) => {
        const departmentSelected = listDepartment.find(item => item.id === value);
        const departmentName = departmentSelected.name;
        setDepartment({
            ...departmentSelected,
            id: generateDepartmentId(departmentName)
        });
        setName(departmentName);
    };
    
    const onChangeDeparmentInfo = (field) => (e) => {
        setDepartment({
            ...department,
            [field]: e.target.value
        })
    };

    const onSaveDepartmentDrawer = () => {
        onSaveNodeDrawer({ department });
    };

    return (
        <NodeDrawerContainer
            title="Thông tin phòng ban"
            visible={true}
            onClose={onClose}
            onSave={onSaveDepartmentDrawer}
            content={(
                <DepartmentDrawerContent
                    listDepartmentSource={listDepartmentSource}
                    onChangeName={onChangeName}
                    name={name}
                    onSelectDepartment={onSelectDepartment}
                    department={department}
                    onChangeDeparmentInfo={onChangeDeparmentInfo}
                />
            )}
        />
    );
};

const DepartmentDrawerContent = ({
    listDepartmentSource,
    onChangeName,
    name,
    onSelectDepartment,
    department = {},
    onChangeDeparmentInfo
}) => {
    return (
        <div className={styles['info-container']}>
            <AutoComplete
                value={name}
                dataSource={listDepartmentSource}
                onSelect={onSelectDepartment}
                // onSearch={onSearch}
                onChange={onChangeName}
                placeholder="Tên phòng ban"
                className={styles['department-name']}
            />
            <Input
                placeholder="Mã phòng ban"
                onChange={onChangeDeparmentInfo('id')}
                className={styles['info-input']}
                value={department.id}
            />
            <Input.TextArea
                placeholder="Mô tả phòng ban"
                className={styles['info-input']}
                onChange={onChangeDeparmentInfo('description')}
                value={department.description}
                rows={5}
            />
        </div>
    )
}

export default DepartmentDrawer;