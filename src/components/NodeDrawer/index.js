import React from 'react';
import { get } from 'lodash';

import RoleDrawer from './RoleDrawer';
import DepartmentDrawer from './DepartmentDrawer';
import {
    DRAWER_ROLE,
    DRAWER_DEPARTMENT,
    DRAWER_ASSISTANT
} from '../../variables/orgChart';

const NodeDrawer = ({
    nodeDrawerData,
    toggleNodeDrawer,
    listUser,
    onSaveNodeDrawer
}) => {
    const onClose = () => {
        toggleNodeDrawer()
    };

    const typeDrawer = get(nodeDrawerData, 'type');

    if (typeDrawer === DRAWER_ROLE || typeDrawer === DRAWER_ASSISTANT) return (
        <RoleDrawer
            onClose={onClose}
            nodeDrawerData={nodeDrawerData}
            listUser={listUser}
            onSaveNodeDrawer={onSaveNodeDrawer}
        />
    )
    if (typeDrawer === DRAWER_DEPARTMENT) return (
        <DepartmentDrawer
            onClose={onClose}
            nodeDrawerData={nodeDrawerData}
            listUser={listUser}
            onSaveNodeDrawer={onSaveNodeDrawer}
        />
    )
    return null;
};

export default NodeDrawer;
