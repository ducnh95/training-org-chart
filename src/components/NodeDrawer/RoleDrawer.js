import React, { useState, useEffect } from 'react';
import { Col, Input, Select, Button } from 'antd';

import HeaderList from '../../components/HeaderList';
import NodeDrawerContainer from '../NodeDrawerContainer';
import styles from './styles.module.scss';
import listPermission from '../../fakeData/listPermission';
import {
    ACTION_ADD_ROLE,
    ACTION_EDIT_ROLE,
    DRAWER_ROLE,
    DRAWER_ASSISTANT,
    ACTION_ADD_ASSISTANT,
    ACTION_EDIT_ASSISTANT
} from '../../variables/orgChart'

const { Option } = Select;

const RoleDrawer = ({
    onClose,
    nodeDrawerData,
    listUser,
    onSaveNodeDrawer
}) => {
    const { node, action, type } = nodeDrawerData;
    const [name, setName] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [userId, setUserId] = useState();
    const [user, setUser] = useState();
    const [title, setTitle] = useState();

    useEffect(() => {
        if (type === DRAWER_ROLE) {
            setTitle('Thông tin công việc');
        }
        if (type === DRAWER_ASSISTANT) {
            setTitle('Thông tin Trợ lý');
        }
    }, [type]);

    useEffect(() => {
        if ((action === ACTION_EDIT_ROLE || action === ACTION_EDIT_ASSISTANT ) && node) {
            const { name, user } = node;
            setName(name);
            if (user) {
                setUser(user);
                setUserId(user.id);
            }
        }
        if (action === ACTION_ADD_ROLE || action === ACTION_ADD_ASSISTANT) {
            setName('');
            setUser();
            setUserId();
        }
    },[node, action])

    const onChangeName = (e) => {
        setName(e.target.value);
    };

    const onSelectUser = (value) => {
        setSearchValue('');
        setUserId(value ? value : undefined);
        setUser(listUser.find(item => item.id === value));
    };

    const onAddNewUserClick = (e) => {
    };

    const onSearch = (value) => {
        setSearchValue(value);
    };

    const onChangeUserInfo = (field) => (e) => {
        setUser({
            ...user,
            [field]: e.target.value
        })
    };

    const onSelectUserPermission = (permissionId) => {
        setUser({
            ...user,
            permission: listPermission.find(permission => permission.id === permissionId)
        });
    };

    const onSaveRoleDrawer = () => {
        onSaveNodeDrawer({ name, user });
    };

    return (
        <NodeDrawerContainer
            title={title}
            visible={true}
            onClose={onClose}
            content={(
                <RoleDrawerContent
                    listUser={listUser}
                    name={name}
                    searchValue={searchValue}
                    userId={userId}
                    user={user}
                    onChangeName={onChangeName}
                    onSelectUser={onSelectUser}
                    onAddNewUserClick={onAddNewUserClick}
                    onSearch={onSearch}
                    onChangeUserInfo={onChangeUserInfo}
                    onSelectUserPermission={onSelectUserPermission}
                />
            )}
            onSave={onSaveRoleDrawer}
        />
    );
};

const RoleDrawerContent = ({
    listUser,
    name,
    searchValue,
    userId,
    user,
    onChangeName,
    onSelectUser,
    onAddNewUserClick,
    onSearch,
    onChangeUserInfo,
    onSelectUserPermission
}) => {
    return (
        <>
            <div className={styles['input-container']}>
                <Input
                    placeholder="Vị trí công việc"
                    onChange={onChangeName}
                    value={name}
                />
                <Select
                    showSearch
                    placeholder="Chọn nhân viên"
                    optionFilterProp="name"
                    onSelect={onSelectUser}
                    onSearch={onSearch}
                    filterOption={(input, option) => {
                        return option.props.name.toLowerCase().includes(input.toLowerCase())
                    }}
                    dropdownRender={menu => (
                        <div>
                            <Button
                                type="link"
                                onClick={onAddNewUserClick}
                                onMouseDown={e => e.preventDefault()}
                                className="add-user-btn"
                            >
                                + Thêm người dùng mới
                            </Button>
                            {menu}
                        </div>
                    )}
                    className={styles['select-drawer']}
                    optionLabelProp={'data-show'}
                    value={userId}
                >
                    <Option
                        value={null}
                        className={styles["option-item-null"]}
                        name="Vị trí trống"
                    >
                        Vị trí trống
                    </Option>
                    {listUser.map(user => (
                        <Option
                            value={user.id}
                            name={user.name}
                            className={styles["option-item"]}
                            key={user.id}
                            data-show={(
                                <div
                                    className={styles['item-selected-container']}
                                    style={{
                                        display: searchValue === '' ? 'flex' : 'none'
                                    }}
                                >
                                    <img className={styles['user-avatar-selected']} src={user.avatar} alt="avatar" />
                                    <div className={styles['user-name-selected']}>
                                        {user.name}
                                    </div>
                                </div>
                            )}
                        >
                            <img className={styles['user-avatar']} src={user.avatar} alt="avatar" />
                            <Col>
                                <div className={styles['user-name']}>
                                    {user.name}
                                </div>
                                <div className={styles['user-role']}>
                                    {user.role}
                                </div>
                            </Col>
                        </Option>
                    ))}
                </Select>
            </div>
            <HeaderList title="THÔNG TIN NHÂN VIÊN" className={styles['header-info']} />
            {userId && (
                <div className={styles['info-container']}>
                    <Input
                        placeholder="Tên nhân viên"
                        value={user.name}
                        disabled
                    />
                    <Input
                        placeholder="Mã nhân viên"
                        value={user.id}
                        onChange={onChangeUserInfo('id')}
                        className={styles['info-input']}
                    />
                    <Input
                        placeholder="Email"
                        value={user.email}
                        disabled
                        className={styles['info-input']}
                    />
                    <Select
                        onSelect={onSelectUserPermission}
                        className={styles['select-drawer']}
                        value={user.accessPermissions.id}
                    >
                        {listPermission.map(item => (
                            <Option value={item.id} key={item.id}>
                                {item.name}
                            </Option>
                        ))}
                    </Select>
                </div>
            )}
        </>
    )
}

export default RoleDrawer;