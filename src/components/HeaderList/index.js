import React from 'react';
import { any } from 'prop-types';

import styles from './styles.module.scss';

const HeaderList = ({ title, className }) => {
    return (
        <div className={className ? className : styles['title']}>
            {title}
        </div>
    )
}

HeaderList.propTypes = {
    title: any
}

export default HeaderList;
