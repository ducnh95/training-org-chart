import React from 'react'
import { useDrag, DragPreviewImage } from 'react-dnd'

import dragPreview from '../../../fakeData/drag-preview.svg';

const style = {
  border: '1px dashed gray',
  backgroundColor: 'white',
  padding: '0.5rem 1rem',
  marginRight: '1.5rem',
  marginBottom: '1.5rem',
  cursor: 'move',
  float: 'left',
}
const Box = ({ name }) => {
  const [{ isDragging }, drag, preview] = useDrag({
    item: { name, type: 'box' },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult()
      if (item && dropResult) {
        alert(`You dropped ${item.name} into ${dropResult.name}!`)
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })
  const opacity = isDragging ? 0.4 : 1
  return (
    <>
     {/* <DragPreviewImage connect={preview} src={dragPreview} /> */}
    <div ref={drag} style={{ ...style, opacity }}>
      {name}
    </div>
    </>
  )
}
export default Box
