import React, { useState } from 'react';
import { Row, Col, Button, Empty, Popover, Menu } from 'antd';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { orderBy } from 'lodash';
import { useDrop } from 'react-dnd';
import cx from 'classnames';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';

import RoleDiv from './RoleDiv';
import styles from './styles.module.scss';
import {
    ORG_CHART_NODE_TYPE,
    ORG_CHART_NODE_EXIST_TYPE,
    TYPE_ROLE,
    TYPE_ASSISTANT,
    TYPE_DEPARTMENT,
    TYPE_USER,
    DRAWER_ASSISTANT,
    DRAWER_ROLE,
    ACTION_ADD_ASSISTANT,
    ACTION_ADD_ROLE,
    KEY_ADD_ASSISTANT,
    KEY_ADD_ROLE
} from '../../variables/orgChart';

const listTypeChildAccept = [TYPE_ROLE, TYPE_ASSISTANT, TYPE_USER];

const MenuToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_ADD_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_ADD_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm trợ lý</div>
            </Menu.Item>
            
       
        </Menu>
    )
};

const ListChildWithoutDepartment = ({
    listNodeWithoutDepartment,
    toggleNodeDrawer,
    showListChildDrawer
}) => {
    const [showAllChild, setShowAllChild] = useState(false);

    const onToggleAllChild = () => {
        setShowAllChild(!showAllChild);
    };

    const [{ isOver }, drop] = useDrop({
        accept: ORG_CHART_NODE_TYPE,
        drop: (item, monitor) => {
            const { nodeType } = item;
            if (listTypeChildAccept.includes(nodeType)) {
                // add new node
                const isAssistant = nodeType === TYPE_ASSISTANT;
                const newItem = {
                    ...item,
                    nodeType: isAssistant ? TYPE_ASSISTANT : TYPE_ROLE
                };
                const drawerType = isAssistant ? DRAWER_ASSISTANT : DRAWER_ROLE;
                const drawerAction = isAssistant ? ACTION_ADD_ASSISTANT : ACTION_ADD_ROLE;
                toggleNodeDrawer({
                    type: drawerType,
                    node: showListChildDrawer,
                    action: drawerAction,
                    newItem
                })
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType } = item;
                const isOver = monitor.isOver();
                return ({
                    isOver: isOver && listTypeChildAccept.includes(nodeType)
                })
            }
            return {
                isOver: false
            }
        }
    })

    const [menuVisible, setMenuVisible] = useState(false);
    const onMenuClick = ({ item, key, domEvent }) => {
        domEvent.stopPropagation();
        setMenuVisible(false);
        switch (key) {
            case KEY_ADD_ROLE:
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: showListChildDrawer,
                    action: ACTION_ADD_ROLE
                })
                break;
            case KEY_ADD_ASSISTANT:
                toggleNodeDrawer({
                    type: DRAWER_ASSISTANT,
                    node: showListChildDrawer,
                    action: ACTION_ADD_ASSISTANT
                })
                break;
            default:
                break;
        }
    };

    const onVisiblePopoverChange = (visible) => {
        setMenuVisible(visible);
    };

    const onPopoverClick = (e) => {
        e.stopPropagation();
    };

    const length = listNodeWithoutDepartment.length;

    return (
        <>
            <div ref={drop}>
                <Row
                    type="flex"
                    align="middle"
                    className={cx(
                        styles['title-container'],
                        {
                            [styles['title-container-hover']]: isOver
                        }
                    )}
                    onClick={onToggleAllChild}
                >
                    {showAllChild ? (
                        <ExpandLessIcon className={styles['expand-icon']} />
                    ) : (
                            <ExpandMoreIcon className={styles['expand-icon']} />
                        )}
                    <span>
                        Nhân viên không có phòng ban
                </span>
                    <Col className={styles['toolbar']}>
                        <Button className={styles['num-of-child-btn']}>
                            {length}
                        </Button>
                        <Popover
                            content={<MenuToolbar onMenuClick={onMenuClick} />}
                            trigger="click"
                            placement="bottom"
                            overlayClassName="popover-node"
                            visible={menuVisible}
                            onVisibleChange={onVisiblePopoverChange}
                            onClick={onPopoverClick}
                        >
                            <MoreHorizIcon className={styles['icon-btn']} />
                        </Popover>
                    </Col>
                </Row>
            </div>

            {showAllChild && (
                (length > 0) ? (
                    <Col className={styles['list-child']}>
                        {orderBy(listNodeWithoutDepartment, 'order', 'desc').map((item) => (
                            <RoleDiv nodeData={item} key={item.id} />
                        ))}
                    </Col>
                ) : (
                        <Empty
                            description={<span>Chưa có vị trí công việc</span>}
                            className={styles['empty']}
                        />
                    )
            )}
        </>
    )
};

export default ListChildWithoutDepartment;
