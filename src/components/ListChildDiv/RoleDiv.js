import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Menu, Popover } from 'antd';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import { isEmpty } from 'lodash';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import { connect } from 'react-redux';
import { useDrop, useDrag, DragPreviewImage } from 'react-dnd';
import cx from 'classnames';

import dragPreview from '../../fakeData/drag-preview.svg';
import {
    TYPE_ROLE,
    TYPE_ASSISTANT,
    KEY_EDIT_ROLE,
    KEY_DELETE_ROLE,
    KEY_DELETE_USER,
    KEY_ADD_ASSISTANT,
    KEY_EDIT_ASSISTANT,
    KEY_DELETE_ASSISTANT,
    KEY_ADD_ROLE,
    KEY_ADD_DEPARTMENT,
    CHILD_DRAWER_NODE,
    ORG_CHART_NODE_TYPE,
    LEFT,
    TYPE_DEPARTMENT,
    ACTION_ADD_ROLE,
    DRAWER_ASSISTANT,
    DRAWER_ROLE,
    ACTION_ADD_ASSISTANT,
    RIGHT,
    ACTION_EDIT_ASSISTANT,
    ACTION_EDIT_ROLE,
    CHILD_DRAWER_DEPARTMENT,
    TYPE_USER,
    DRAWER_DEPARTMENT,
    ACTION_ADD_DEPARTMENT,
} from '../../variables/orgChart';
import { selectNumberChildOfNode, selectNodeDrawerData } from '../../module/selectors/orgChart';
import styles from './styles.module.scss';
import {
    changeRootNode,
    toggleListChildDrawer,
    setRelativeNode,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga,
    addUserToNodeSaga
} from '../../module/actions/orgChart';

const listTypeChildAccept = [TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT, TYPE_USER];
const listTypeSiblingAccept = [TYPE_ROLE, TYPE_ASSISTANT];

const MenuRoleToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_ADD_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_ADD_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm trợ lý</div>
            </Menu.Item>
            <Menu.Item key={KEY_ADD_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm phòng ban</div>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key={KEY_EDIT_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_USER} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá nguời dùng khỏi vị trí</div>
            </Menu.Item>
        </Menu>
    )
};

const MenuAssistantToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_EDIT_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa trợ lý</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá trợ lý</div>
            </Menu.Item>
        </Menu>
    )
}

const RoleDiv = ({
    nodeData,
    numberChildOfNode,
    changeRootNode,
    toggleListChildDrawer,
    setRelativeNode,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    haveDepartment,
    changeNodePositionToChildrenSaga,
    nodeDrawerData,
    addUserToNodeSaga
}) => {
    const { type, name, user } = nodeData;
    const isAssistant = type === TYPE_ASSISTANT;
    const [menuVisible, setMenuVisible] = useState(false);
    const [active, setActive] = useState(false);

    useEffect(() => {
        if(!nodeDrawerData) {
            setActive(false);
        }
    }, [nodeDrawerData])

    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: CHILD_DRAWER_NODE,
            nodeType: nodeData.type,
            nodeData
        },
        collect: (monitor) => {
            return ({
                isDragging: monitor.isDragging()
            })
        }
    })

    const [{ isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_NODE, CHILD_DRAWER_DEPARTMENT],
        drop: (item, monitor) => {
            const { type: nodeDataType, user: nodeDataUser } = nodeData;
            const { nodeType, type, user } = item;
            const isDropToSibling = monitor.didDrop();
            if (isDropToSibling) {
                return;
            }
            if (nodeDataType === TYPE_ASSISTANT && !isEmpty(nodeDataUser)) {
                return;
            }
            if (type === CHILD_DRAWER_NODE || type === CHILD_DRAWER_DEPARTMENT) {
                return changeNodePositionToChildrenSaga({ item, root: nodeData });
            }
            if (type === ORG_CHART_NODE_TYPE && listTypeChildAccept.includes(nodeType)) {
                if (nodeType === TYPE_USER && isEmpty(nodeDataUser)) {
                    return addUserToNodeSaga({ item, root: nodeData });
                }

                // add new role node
                const newItem = {
                    ...item
                };
                let type, action;
                switch (nodeType) {
                    case TYPE_USER:
                        newItem.nodeType = TYPE_ROLE;
                        type = DRAWER_ROLE;
                        action = ACTION_ADD_ROLE;
                        break;
                    case TYPE_ROLE:
                        type = DRAWER_ROLE;
                        action = ACTION_ADD_ROLE;
                        break;
                    case TYPE_DEPARTMENT:
                        type = DRAWER_DEPARTMENT;
                        action = ACTION_ADD_DEPARTMENT;
                        break;
                    case TYPE_ASSISTANT:
                        type = DRAWER_ASSISTANT;
                        action = ACTION_ADD_ASSISTANT;
                        break;
                    default:
                        break;
                }
                toggleNodeDrawer({
                    type,
                    node: nodeData,
                    action,
                    newItem,
                    isChangeRelativeNode: true,
                    user
                })
                setActive(true);
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            const { type: nodeDataType, user: nodeDataUser } = nodeData;
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                return ({
                    isOver: isOver &&
                        !(nodeDataType === TYPE_ASSISTANT && !isEmpty(nodeDataUser)) && (
                        type === CHILD_DRAWER_NODE ||
                        type === CHILD_DRAWER_DEPARTMENT ||
                        (type === ORG_CHART_NODE_TYPE && listTypeChildAccept.includes(nodeType))
                    )
                })
            }
            return {
                isOver: false
            }
        }
    });

    const [{ isOverSiblingBottom }, dropToSiblingBottom] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_NODE],
        drop: (item, monitor) => {
            const { nodeType, type } = item;
            if (haveDepartment && nodeType === TYPE_ASSISTANT) {
                return;
            }
            if (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)) {
                // add new role node
                const isAssistant = nodeType === TYPE_ASSISTANT;
                toggleNodeDrawer({
                    type: isAssistant ? DRAWER_ASSISTANT : DRAWER_ROLE,
                    sibling: nodeData,
                    action: isAssistant ? ACTION_ADD_ASSISTANT : ACTION_ADD_ROLE,
                    direction: LEFT
                })

            }
            if (type === CHILD_DRAWER_NODE) {
                changeNodePositionToSiblingSaga({
                    item,
                    root: nodeData,
                    direction: LEFT
                })
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                const isCreateAssistantToDepartment = haveDepartment && nodeType === TYPE_ASSISTANT;
                return ({
                    isOverSiblingBottom: isOver && !isCreateAssistantToDepartment && (type === CHILD_DRAWER_NODE || (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)))
                })
            }
            return {
                isOverSiblingBottom: false
            }
        }
    })

    const [{ isOverSiblingTop }, dropToSiblingTop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_NODE],
        drop: (item, monitor) => {
            const { nodeType, type } = item;
            if (haveDepartment && nodeType === TYPE_ASSISTANT) {
                return;
            }
            if (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)) {
                // add new role node
                const isAssistant = nodeType === TYPE_ASSISTANT;
                toggleNodeDrawer({
                    type: isAssistant ? DRAWER_ASSISTANT : DRAWER_ROLE,
                    sibling: nodeData,
                    action: isAssistant ? ACTION_ADD_ASSISTANT : ACTION_ADD_ROLE,
                    direction: RIGHT
                })

            }
            if (type === CHILD_DRAWER_NODE) {
                changeNodePositionToSiblingSaga({
                    item,
                    root: nodeData,
                    direction: RIGHT
                })
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                const isCreateAssistantToDepartment = haveDepartment && nodeType === TYPE_ASSISTANT;
                return ({
                    isOverSiblingTop: isOver && !isCreateAssistantToDepartment && (type === CHILD_DRAWER_NODE || (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)))
                })
            }
            return {
                isOverSiblingTop: false
            }
        }
    })

    const onMenuClick = ({ item, key, domEvent }) => {
        domEvent.stopPropagation();
        setMenuVisible(false);
    };

    const onVisiblePopoverChange = (visible) => {
        setMenuVisible(visible);
    };

    const onChangeRoot = (e) => {
        e.stopPropagation();
        changeRootNode(nodeData);
        toggleListChildDrawer({});
    };

    const onShowRelativeNode = (e) => {
        e.stopPropagation();
        setRelativeNode(nodeData);
    };

    const onNodeClick = () => {
        setActive(!active);
        const { nodeType } = nodeData;
        const isAssistant = nodeType === TYPE_ASSISTANT;
        toggleNodeDrawer({
            type: isAssistant ? DRAWER_ASSISTANT : DRAWER_ROLE,
            node: nodeData,
            action: isAssistant ? ACTION_EDIT_ASSISTANT : ACTION_EDIT_ROLE
        })
    }

    const onPopoverClick = (e) => {
        e.stopPropagation();
    };

    const opacity = isDragging ? '0.4' : '';

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <Col>
                <div
                    ref={dropToSiblingTop}
                    className={styles['top-bar-drop-node']}
                >
                    {isOverSiblingTop && (
                        <div className={styles['top-bar-drop-node-hover']} />
                    )}
                </div>
                <div ref={drag} style={{ opacity }}>
                    <div ref={drop}>
                        <Row
                            type="flex"
                            align={!!user ? "middle" : undefined}
                            onClick={onNodeClick}
                            className={cx(
                                styles['item-detail'],
                                {
                                    [styles['item-detail-drop']]: active || (isOver && !isOverSiblingBottom && !isOverSiblingTop)
                                }
                            )}
                        >
                            <Col>
                                <Row type="flex">
                                    {isAssistant && (
                                        <AccessibilityIcon className="mr-2" />
                                    )}
                                    <span>
                                        {name}
                                    </span>
                                </Row>
                                {!isEmpty(user) && (
                                    <Row type="flex" className="mt-2" align="middle">
                                        <img className={styles['info-avatar']} src={user.avatar} alt="info-avatar" />
                                        <Col>
                                            <div>
                                                {user.name}
                                            </div>
                                            <div className={styles['secondary-text']}>
                                                {user.id}
                                            </div>
                                        </Col>
                                    </Row>
                                )}
                            </Col>
                            <Row type="flex" className={styles['toolbar']} align="middle">
                                {isAssistant ? (
                                    <Popover
                                        content={<MenuAssistantToolbar onMenuClick={onMenuClick} />}
                                        trigger="click"
                                        placement="bottom"
                                        overlayClassName="popover-node"
                                        visible={menuVisible}
                                        onVisibleChange={onVisiblePopoverChange}
                                        onClick={onPopoverClick}
                                    >
                                        <MoreHorizIcon className={styles['icon-btn']} />
                                    </Popover>
                                ) : (
                                        <>
                                            <Button className={styles['num-of-child-btn']} onClick={onShowRelativeNode}>
                                                {numberChildOfNode}
                                            </Button>
                                            <MyLocationIcon className={styles['icon-btn']} onClick={onChangeRoot} />
                                            <Popover
                                                content={<MenuRoleToolbar onMenuClick={onMenuClick} />}
                                                trigger="click"
                                                placement="bottom"
                                                overlayClassName="popover-node"
                                                visible={menuVisible}
                                                onVisibleChange={onVisiblePopoverChange}
                                                onClick={onPopoverClick}
                                            >
                                                <MoreHorizIcon className={styles['icon-btn']} />

                                            </Popover>
                                        </>
                                    )}
                            </Row>
                        </Row>
                    </div>

                </div>
                <div
                    ref={dropToSiblingBottom}
                    className={styles['bottom-bar-drop-node']}
                >
                    {isOverSiblingBottom && (
                        <div className={styles['bottom-bar-drop-node-hover']} />
                    )}
                </div>
                <div className={styles['divider']} />
            </Col>
        </>
    )
};

export default connect((state, ownProps) => ({
    numberChildOfNode: selectNumberChildOfNode(ownProps.nodeData)(state),
    nodeDrawerData: selectNodeDrawerData(state)
}), {
    changeRootNode,
    toggleListChildDrawer,
    setRelativeNode,
    toggleNodeDrawer,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga,
    addUserToNodeSaga
})(RoleDiv);