import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Popover, Menu, Empty } from 'antd';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { get, isEmpty, orderBy } from 'lodash';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import cx from 'classnames';
import { useDrop, useDrag, DragPreviewImage } from 'react-dnd';
import dragPreview from '../../fakeData/drag-preview.svg';

import {
    KEY_EDIT_DEPARTMENT,
    KEY_DELETE_DEPARTMENT,
    KEY_ADD_ROLE,
    TYPE_ROLE,
    TYPE_USER,
    ORG_CHART_NODE_TYPE,
    DRAWER_ROLE,
    ACTION_ADD_ROLE,
    DRAWER_DEPARTMENT,
    ACTION_EDIT_DEPARTMENT,
    TYPE_DEPARTMENT,
    ACTION_ADD_DEPARTMENT,
    RIGHT,
    LEFT,
    CHILD_DRAWER_DEPARTMENT,
    CHILD_DRAWER_NODE,
    TYPE_ASSISTANT
} from '../../variables/orgChart';
import RoleDiv from './RoleDiv';
import styles from './styles.module.scss';
import { getNumberChildren } from '../../utils';

const listTypeChildAccept = [TYPE_ROLE, TYPE_USER];
const listTypeSiblingAccept = [TYPE_DEPARTMENT];

const MenuDepartmentToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_ADD_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm vị trí công việc</div>
            </Menu.Item>
            <Menu.Divider className="mt-n1" />
            <Menu.Item key={KEY_EDIT_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa phòng ban</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá phòng ban</div>
            </Menu.Item>
        </Menu>
    )
};

const ListChildWithDepartment = ({
    nodeData,
    toggleNodeDrawer,
    showListChildDrawer,
    isLastItem,
    changeNodePositionToSiblingSaga,
    changeNodePositionToChildrenSaga
}) => {
    const [showAllChild, setShowAllChild] = useState(false);
    const [menuVisible, setMenuVisible] = useState(false);

    const [{ isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_NODE],
        drop: (item, monitor) => {
            const { nodeType, type } = item;
            const isDropToSibling = monitor.didDrop();
            if (isDropToSibling) {
                return;
            }
            if (type === CHILD_DRAWER_NODE) {
                if (nodeType === TYPE_ASSISTANT) {
                    // cann't move assistant to department
                    return;
                }
                return changeNodePositionToChildrenSaga({ item, root: nodeData });
            }
            if (type === ORG_CHART_NODE_TYPE && listTypeChildAccept.includes(nodeType)) {
                // add new role node
                const newItem = {
                    ...item,
                    nodeType: TYPE_ROLE
                };
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: nodeData,
                    action: ACTION_ADD_ROLE,
                    newItem
                })
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                return ({
                    isOver: isOver && ((type === CHILD_DRAWER_NODE && nodeType !== TYPE_ASSISTANT) || (type === ORG_CHART_NODE_TYPE && listTypeChildAccept.includes(nodeType)))
                })
            }
            return {
                isOver: false
            }
        }
    });

    const [{ isOverSiblingTop }, dropToSiblingTop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_DEPARTMENT],
        drop: (item, monitor) => {
            const { type, nodeType } = item;
            if (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)) {
                // add new role node
                toggleNodeDrawer({
                    type: DRAWER_DEPARTMENT,
                    sibling: nodeData,
                    action: ACTION_ADD_DEPARTMENT,
                    direction: RIGHT
                })
            }
            if (type === CHILD_DRAWER_DEPARTMENT) {
                changeNodePositionToSiblingSaga({
                    item,
                    root: nodeData,
                    direction: RIGHT
                })
            }

        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                return ({
                    isOverSiblingTop: isOver && (type === CHILD_DRAWER_DEPARTMENT || (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)))
                })
            }
            return {
                isOverSiblingTop: false
            }
        }
    });

    const [{ isOverSiblingBottom }, dropToSiblingBottom] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, CHILD_DRAWER_DEPARTMENT],
        drop: (item, monitor) => {
            const { nodeType, type } = item;
            if (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)) {
                // add new role node
                toggleNodeDrawer({
                    type: DRAWER_DEPARTMENT,
                    sibling: nodeData,
                    action: ACTION_ADD_DEPARTMENT,
                    direction: LEFT
                })
            }
            if (type === CHILD_DRAWER_DEPARTMENT) {
                changeNodePositionToSiblingSaga({
                    item,
                    root: nodeData,
                    direction: LEFT
                })
            }
        },
        collect: (monitor) => {
            const item = monitor.getItem();
            if (item) {
                const { nodeType, type } = item;
                const isOver = monitor.isOver();
                return ({
                    isOverSiblingBottom: isOver && (type === CHILD_DRAWER_DEPARTMENT || (type === ORG_CHART_NODE_TYPE && listTypeSiblingAccept.includes(nodeType)))
                })
            }
            return {
                isOverSiblingBottom: false
            }
        }
    })

    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: CHILD_DRAWER_DEPARTMENT,
            nodeType: nodeData.type,
            nodeData
        },
        collect: (monitor) => {
            return ({
                isDragging: monitor.isDragging()
            })
        }
    })

    const onMenuClick = ({ item, key, domEvent }) => {
        domEvent.stopPropagation();
        setMenuVisible(false);
        switch (key) {
            case KEY_ADD_ROLE:
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: nodeData,
                    action: ACTION_ADD_ROLE
                })
                break;
            case KEY_EDIT_DEPARTMENT:
                toggleNodeDrawer({
                    type: DRAWER_DEPARTMENT,
                    node: nodeData,
                    action: ACTION_EDIT_DEPARTMENT
                })
                break;
            default:
                break;
        }
    };

    const onVisiblePopoverChange = (visible) => {
        setMenuVisible(visible);
    };

    const departmentName = get(nodeData, 'department.name');
    const departmentId = get(nodeData, 'department.id');

    const onToggleAllChild = () => {
        setShowAllChild(!showAllChild);
    };

    const onPopoverClick = (e) => {
        e.stopPropagation();
    };

    if (isEmpty(nodeData)) {
        return null;
    }

    const numberChildren = getNumberChildren(nodeData);
    const opacity = isDragging ? '0.4' : '';

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <div ref={drag} style={{ opacity, position: 'relative' }}>
                <>
                    <div
                        ref={dropToSiblingTop}
                        className={styles['top-bar-drop']}
                    >
                        {isOverSiblingTop && (
                            <div className={styles['top-bar-drop-hover']} />
                        )}
                    </div>
                    <div ref={drop}>
                        <Row
                            type="flex"
                            align="middle"
                            className={cx(
                                styles['title-container'],
                                styles['title-department-container'],
                                {
                                    [styles['title-department-container-hover']]: isOver && !isOverSiblingTop && !isOverSiblingBottom
                                }
                            )}
                            onClick={onToggleAllChild}
                        >

                            <div className={cx(styles['expand-icon'], 'd-flex', 'flex-column')}>
                                <WorkOutlineIcon className={styles['department-icon']} />
                                {showAllChild ? (
                                    <ExpandLessIcon className={cx(styles['department-icon'], 'mt-1')} />
                                ) : (
                                        <ExpandMoreIcon className={cx(styles['department-icon'], 'mt-1')} />
                                    )}
                            </div>
                            <Col>
                                <div>
                                    {departmentName}
                                </div>
                                <div className={styles['secondary-text']}>
                                    {departmentId}
                                </div>
                            </Col>
                            <Col className={styles['toolbar']}>
                                <Button className={styles['num-of-child-btn']}>
                                    {numberChildren}
                                </Button>
                                <Popover
                                    content={<MenuDepartmentToolbar onMenuClick={onMenuClick} />}
                                    trigger="click"
                                    placement="bottom"
                                    overlayClassName="popover-node"
                                    visible={menuVisible}
                                    onVisibleChange={onVisiblePopoverChange}
                                    onClick={onPopoverClick}
                                >
                                    <MoreHorizIcon className={styles['icon-btn']} />
                                </Popover>
                            </Col>
                        </Row>
                    </div>

                    {isLastItem && (
                        <div
                            ref={dropToSiblingBottom}
                            className={styles['top-bar-drop']}
                        >
                            {isOverSiblingBottom && (
                                <div className={styles['top-bar-drop-hover']} />
                            )}
                        </div>
                    )}
                    {showAllChild && (
                        (numberChildren > 0) ? (
                            <Col className={styles['list-child']}>
                                {orderBy(Object.values(nodeData.children), 'order', 'desc').map((item) => (
                                    <RoleDiv nodeData={item} key={item.id} haveDepartment={true} />
                                ))}
                            </Col>
                        ) : (
                                <Empty
                                    description={<span>Chưa có vị trí công việc</span>}
                                    className={styles['empty']}
                                />
                            )
                    )}
                </>
            </div>
        </>
    )
};

export default ListChildWithDepartment;
