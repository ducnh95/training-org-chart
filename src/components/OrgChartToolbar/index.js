import React, { useCallback } from 'react';
import { Input, Button, Icon } from 'antd';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

import styles from './styles.module.scss';

const { Search } = Input;
const { Group } = Button;

const OrgChartToolbar = ({
    onIncreasePercentChart,
    onDecreasePercentChart,
    onSetDefaultPercentChart,
    percentChart,
    rootNode,
    relativeNode,
    changeRootNode,
    setRelativeNode
}) => {
    const onIncreaseClick = () => {
        onIncreasePercentChart();
    };

    const onDecreaseClick =() => {
        onDecreasePercentChart();
    };

    const onPercentClick = () => {
        onSetDefaultPercentChart();
    };

    const onToggleCollapseClick = () => {
    };

    const onBackAllChartclick = () => {
        changeRootNode(null);
        setRelativeNode();
    };

    return (
        <div className={styles['container']}>
            <Input
                placeholder="Nhập tên người dùng hoặc phòng ban"
                prefix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />}
                className={styles['search-box']}
            />
            <div className={styles['zoom-container']}>
                <Group>
                    <Button
                        icon="minus"
                        className={styles['zoom-btn']}
                        onClick={onDecreaseClick}
                    />
                    <Button
                        className={styles['percent-btn']}
                        onClick={onPercentClick}
                    >
                        {`${percentChart} %`}
                    </Button>
                    <Button
                        icon="plus"
                        className={styles['zoom-btn']}
                        onClick={onIncreaseClick}
                    />
                </Group>
            </div>
            <Button className={styles['collapse-btn']} onClick={onToggleCollapseClick}>
                    <ArrowDropUpIcon />
            </Button>
            {/* <Button className={styles['collapse-btn']} onClick={onToggleCollapseClick}>
                    <ArrowDropDownIcon />
            </Button> */}
            {(!!rootNode || !!relativeNode) && (
                <Button className={styles['back-all-chart']} onClick={onBackAllChartclick}>
                    Quay lại xem tất cả
                </Button>
            )}
        </div>
    )
};

export default OrgChartToolbar;
