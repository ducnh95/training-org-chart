import React from 'react';
import { useDrag, DragPreviewImage } from 'react-dnd';
import { object } from 'prop-types';

import dragPreview from '../../fakeData/drag-preview.svg';
import { ORG_CHART_NODE_TYPE, TYPE_USER } from '../../variables/orgChart';
import styles from './styles.module.scss';

const UserInfo = ({ user }) => {
    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: ORG_CHART_NODE_TYPE,
            nodeType: TYPE_USER,
            user
        },
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();
            if (item && dropResult) {
            }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    })

    const opacity = isDragging ? 0.4 : 1;

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <div className={styles['user-info']} style={{ opacity }} ref={drag}>
                <img className={styles['user-avatar']} src={user.avatar} alt="avatar" />
                <div className={styles['user-name']}>
                    {user.name}
                </div>
            </div>
        </>
    )
};

UserInfo.propTypes = {
    user: object
};

export default UserInfo;
