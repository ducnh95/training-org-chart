import React, { useState, useCallback } from 'react';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import AssistantIcon from '@material-ui/icons/AssistantOutlined';
import { Button } from 'antd';
import cx from 'classnames';

import HeaderList from '../HeaderList';
import OrgChartNodeType from '../OrgChartNodeType';
import styles from './styles.module.scss';
import ListUserSearchable from './ListUserSearchable';
import { TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT } from '../../variables/orgChart';

const Sidebar = ({ listUserShow }) => {
    const [showSidebar, setShowSidebar] = useState(true);

    const onToggleSidebar = useCallback(() => {
        setShowSidebar(!showSidebar);
    }, [showSidebar])

    return (
        <aside className={cx(
            styles['sidebar'],
            {
                [styles['sidebar-hide']]: !showSidebar
            }
        )}>
            {showSidebar && (
                <>
                    <HeaderList title="TẠO MỚI" />
                    <div className={styles['listType']}>
                        <OrgChartNodeType
                            icon={<WorkOutlineIcon style={{ fontSize: '20px' }} />}
                            label="Công việc"
                            nodeType={TYPE_ROLE}
                        />
                        <OrgChartNodeType
                            icon={<HomeIcon style={{ fontSize: '20px' }} />}
                            label="Trợ lý"
                            nodeType={TYPE_ASSISTANT}
                        />
                        <OrgChartNodeType
                            icon={<AssistantIcon style={{ fontSize: '20px' }} />}
                            label="Phòng ban"
                            nodeType={TYPE_DEPARTMENT}
                        />
                    </div>
                    <ListUserSearchable listUserShow={listUserShow} />
                </>
            )}
            <Button
                icon={showSidebar ? "left" : "right"}
                className={styles['btn-toggle-sidebar']}
                onClick={onToggleSidebar}
            />
        </aside>
    )
}

export default Sidebar;