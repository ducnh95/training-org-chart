import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import { func } from 'prop-types';
import { Button, Tooltip } from 'antd';

import styles from './styles.module.scss';

const ListUserSearchBar = ({ onShowSearchInput }) => {
    return (
        <div className={styles['searchbar-container']}>
            <div className={styles['searchbar-title']}>
                DANH SÁCH NGƯỜI DÙNG
            </div>
            <div className={styles['searchbar-toolbar']}>
            <Tooltip title="Tìm kiếm">
                <SearchIcon className={styles['icon-btn']} onClick={onShowSearchInput} />
            </Tooltip>
            <Tooltip title="Thêm người dùng">
                <AddIcon className={styles['icon-btn']} />
            </Tooltip>
            </div>
        </div>
    )
}

ListUserSearchBar.propTypes = {
    onShowSearchInput: func
}

export default ListUserSearchBar;
