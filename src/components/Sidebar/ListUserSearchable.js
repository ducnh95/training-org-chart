import React, { useState, useEffect, useCallback } from 'react';
import { Input } from 'antd';


import ListUserSearchBar from './ListUserSearchBar';
import HeaderList from '../HeaderList';
import UserInfo from './UserInfo';
import styles from './styles.module.scss';

const ListUserSearchable = ({ listUserShow }) => {
    const [searching, setSearching] = useState(false);
    const [searchValue, setSearchValue] = useState('');
    // const [listUserShow, setListUserShow] = useState(listUserData);
    // useEffect(() => {
    //     const newListUserShow = listUserData.filter(user => user.name.toLowerCase().includes(searchValue.toLowerCase()));
    //     setListUserShow(newListUserShow);
    // }, [searchValue]);

    const onShowSearchInput = useCallback(() => {
        setSearching(!searching);
    }, [searching])

    const onInputChange = useCallback((e) => {
        setSearchValue(e.target.value);
    }, []);

    return (
        <>
            <HeaderList title={<ListUserSearchBar onShowSearchInput={onShowSearchInput} />} />
            <div className={styles['list-user-container']}>
                {searching && (
                    <div className={styles['search-form']}>
                        <Input onChange={onInputChange} autoFocus value={searchValue} />
                    </div>
                )}
                <div className={styles['list-user']}>
                    {listUserShow.map(user => (
                            <UserInfo user={user} key={user.id} />
                        )
                    )}
                </div>
            </div>
        </>
    )
}

export default ListUserSearchable;