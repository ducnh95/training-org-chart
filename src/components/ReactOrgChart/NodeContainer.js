import React from 'react';
import { object, func } from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import {
    selectListNodeCollapse,
    selectNumberChildOfNode,
    selectListNodeDraggingId,
    selectRelativeNode,
    selectNumberDepartment
} from '../../module/selectors/orgChart';
import {
    toggleCollapseNode,
    addChildNodeSaga,
    addUserToNodeSaga,
    addSiblingNodeSaga,
    changeNodePositionToChildrenSaga,
    changeNodePositionToSiblingSaga,
    changeRootNode,
    setNodeDraggingId,
    toggleListChildDrawer,
    toggleNodeDrawer
} from '../../module/actions/orgChart';

import { TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT, TYPE_MULTI_USER } from '../../variables/orgChart';

import RoleNode from './RoleNode';
import DepartmentNode from './DepartmentNode';
import AssistantNode from './AssistantNode';
import MultiRoleNode from './MultiRoleNode';

const NodeContainer = ({
    nodeData,
    listNodeCollapse,
    toggleCollapseNode,
    nodeProps,
    addChildNodeSaga,
    addUserToNodeSaga,
    addSiblingNodeSaga,
    numberChildOfNode,
    changeNodePositionToChildrenSaga,
    changeNodePositionToSiblingSaga,
    changeRootNode,
    setNodeDraggingId,
    listNodeDraggingId,
    toggleListChildDrawer,
    toggleNodeDrawer,
    relativeNode,
    numberDepartment
}) => {
    const { type, id } = nodeData;
    let disabledDnD = false;
    if (!isEmpty(relativeNode)) {
        const relativeParent = relativeNode.parent;
        disabledDnD = relativeParent.includes(id);
    }
    switch (type) {
        case TYPE_ROLE:
            return (
                <RoleNode
                    {...nodeProps}
                    nodeData={nodeData}
                    listNodeCollapse={listNodeCollapse}
                    toggleCollapseNode={toggleCollapseNode}
                    addChildNodeSaga={addChildNodeSaga}
                    addUserToNodeSaga={addUserToNodeSaga}
                    addSiblingNodeSaga={addSiblingNodeSaga}
                    numberChildOfNode={numberChildOfNode}
                    changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
                    changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                    changeRootNode={changeRootNode}
                    setNodeDraggingId={setNodeDraggingId}
                    listNodeDraggingId={listNodeDraggingId}
                    toggleNodeDrawer={toggleNodeDrawer}
                    disabledDnD={disabledDnD}
                    numberDepartment={numberDepartment}
                />
            );
        case TYPE_ASSISTANT:
            return (
                <AssistantNode
                    {...nodeProps}
                    nodeData={nodeData}
                    addUserToNodeSaga={addUserToNodeSaga}
                    addSiblingNodeSaga={addSiblingNodeSaga}
                    changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                    setNodeDraggingId={setNodeDraggingId}
                    listNodeDraggingId={listNodeDraggingId}
                    toggleNodeDrawer={toggleNodeDrawer}
                    disabledDnD={disabledDnD}
                />
            );
        case TYPE_DEPARTMENT:
            return (
                <DepartmentNode
                    {...nodeProps}
                    nodeData={nodeData}
                    addChildNodeSaga={addChildNodeSaga}
                    addSiblingNodeSaga={addSiblingNodeSaga}
                    numberChildOfNode={numberChildOfNode}
                    changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
                    changeNodePositionToSiblingSaga={changeNodePositionToSiblingSaga}
                    setNodeDraggingId={setNodeDraggingId}
                    listNodeDraggingId={listNodeDraggingId}
                    toggleNodeDrawer={toggleNodeDrawer}
                    disabledDnD={disabledDnD}
                />
            )
        case TYPE_MULTI_USER:
            return (
                <MultiRoleNode
                    {...nodeProps}
                    nodeData={nodeData}
                    toggleListChildDrawer={toggleListChildDrawer}
                />
            )
        default:
            return;
    }
}

NodeContainer.propTypes = {
    nodeData: object,
    listNodeCollapse: object,
    toggleCollapseNode: func
};

export default connect((state, ownProps) => ({
    listNodeCollapse: selectListNodeCollapse(state),
    numberChildOfNode: selectNumberChildOfNode(ownProps.nodeData)(state),
    listNodeDraggingId: selectListNodeDraggingId(state),
    relativeNode: selectRelativeNode(state),
    numberDepartment: selectNumberDepartment(ownProps.nodeData)(state)
}), {
    toggleCollapseNode,
    addChildNodeSaga,
    addUserToNodeSaga,
    addSiblingNodeSaga,
    changeNodePositionToChildrenSaga,
    changeNodePositionToSiblingSaga,
    changeRootNode,
    setNodeDraggingId,
    toggleListChildDrawer,
    toggleNodeDrawer
})(NodeContainer);
