import React, { useState, useEffect, useCallback } from 'react';
import { Card, Button, Icon, Popover, Menu, Tooltip, Col, Row } from 'antd';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import AddIcon from '@material-ui/icons/Add';
import { get } from 'lodash';
import cx from 'classnames';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import { useDrop, useDrag, DragPreviewImage } from 'react-dnd';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {
    TYPE_ROLE,
    TYPE_ASSISTANT,
    TYPE_DEPARTMENT,
    ORG_CHART_NODE_TYPE,
    TYPE_USER,
    ORG_CHART_NODE_EXIST_TYPE,
    KEY_ADD_ROLE,
    KEY_ADD_DEPARTMENT,
    KEY_ADD_ASSISTANT,
    KEY_CHANGE_DIRECT_PARENT,
    KEY_EDIT_ROLE,
    KEY_DELETE_ROLE,
    KEY_DELETE_USER,
    MENU_INFERIOR,
    MENU_TOOLBAR,
    DRAWER_DEPARTMENT,
    DRAWER_ROLE,
    DRAWER_ASSISTANT,
    ACTION_ADD_DEPARTMENT,
    ACTION_ADD_ROLE,
    ACTION_ADD_ASSISTANT,
    ACTION_EDIT_ROLE,
    LEFT,
    RIGHT
} from '../../variables/orgChart';
import styles from './styles.module.scss';
import './styles.scss';
import dragPreview from '../../fakeData/drag-preview.svg';
import useDropToSibling from './useDropToSibling';
import { countNumOfDepartment } from '../../utils';

const listTypeChildAccept = [TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT, TYPE_USER];
const listTypeSiblingAccept = [TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT, TYPE_USER];

const MenuAddInferior = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_ADD_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_ADD_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm trợ lý</div>
            </Menu.Item>
            <Menu.Item key={KEY_ADD_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm phòng ban</div>
            </Menu.Item>
        </Menu>
    )
}

const MenuToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_CHANGE_DIRECT_PARENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Đổi quản lý</div>
            </Menu.Item>
            <Menu.Item key={KEY_EDIT_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá vị trí công việc</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_USER} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá nguời dùng khỏi vị trí</div>
            </Menu.Item>
        </Menu>
    )
}

const RoleNode = ({
    nodeData = {},
    listNodeCollapse = {},
    toggleCollapseNode,
    addChildNodeSaga,
    addUserToNodeSaga,
    setNodeDraggingId,
    listNodeDraggingId,
    changeRootNode,
    toggleNodeDrawer,
    addSiblingNodeSaga,
    numberChildOfNode,
    changeNodePositionToChildrenSaga,
    changeNodePositionToSiblingSaga,
    disabledDnD,
    numberDepartment
}) => {
    const [isHover, setIsHover] = useState(false);
    const [alwaysHover, setAlwaysHover] = useState(false);
    const [isCollapsed, setIsCollapsed] = useState(false);
    const [active, setActive] = useState(false);
    const [menuVisible, setMenuVisible] = useState(); // type of menu visible

    const [{ isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        drop: (item, monitor) => {
            const { type } = item;
            if (type === ORG_CHART_NODE_TYPE && listTypeChildAccept.includes(item.nodeType)) {
                // add new node
                if (item.nodeType === TYPE_USER) {
                    if (!nodeData.user) {
                        // add user to node
                        return addUserToNodeSaga({ item, root: nodeData });
                    }
                    // add node and add user to that node
                    return addChildNodeSaga({
                        item: {
                            ...item,
                            nodeType: TYPE_ROLE
                        },
                        root: nodeData,
                    });
                }
                // add empty node
                addChildNodeSaga({
                    item,
                    root: nodeData
                });
            }
            if (type === ORG_CHART_NODE_EXIST_TYPE && listTypeChildAccept.includes(item.nodeType)) {
                // change position of node
                changeNodePositionToChildrenSaga({ item, root: nodeData });
            }

        },
        collect: (monitor) => {
            return ({
                isOver: monitor.isOver()
            })
        }
    })

    const {
        isOverSibling: isOverLeftBar,
        dropToSibling: dropToLeft
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: LEFT
    });

    const {
        isOverSibling: isOverRightBar,
        dropToSibling: dropToRight
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: RIGHT
    });

    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: ORG_CHART_NODE_EXIST_TYPE,
            nodeType: nodeData.type,
            nodeData
        },
        end: (item, monitor) => {
            setNodeDraggingId();
        },
        collect: (monitor) => {
            const isDragging = monitor.isDragging();
            if (isDragging) {
                setNodeDraggingId(nodeData)
            }
            return ({
                isDragging
            })
        }
    })

    const onMouseEnter = () => {
        setIsHover(true);
        setActive(false);
    };

    const onMouseLeave = () => {
        if (!alwaysHover) {
            setIsHover(false);
        }
    };

    const onPopoverClick = () => {
        setAlwaysHover(!alwaysHover)
    };

    const onVisiblePopoverChange = (menuType) => (visible) => {
        if (!visible) {
            // when close popover, turn off always hover
            setAlwaysHover(false);
            setIsHover(false);
            setMenuVisible();
        }
        if (visible) {
            setMenuVisible(menuType);
        }
    };

    const onToggleCollapseClick = () => {
        toggleCollapseNode(nodeData.id)
    };

    useEffect(() => {
        setIsCollapsed(listNodeCollapse[nodeData.id]);
    }, [listNodeCollapse, nodeData.id])

    useEffect(() => {
        if (isDragging) {
            setIsHover(false);
        }
    }, [isDragging])

    const onMyLocationClick = () => {
        changeRootNode(nodeData);
    };

    const { user } = nodeData;

    const isOpacity = listNodeDraggingId.includes(nodeData.id);
    const onMenuClick = (typeMenu) => ({ item, key }) => {
        setMenuVisible();
        setAlwaysHover(false);
        switch (key) {
            case KEY_ADD_ROLE:
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: nodeData,
                    action: ACTION_ADD_ROLE
                })
                setActive(true);
                break;
            case KEY_ADD_DEPARTMENT:
                toggleNodeDrawer({
                    type: DRAWER_DEPARTMENT,
                    node: nodeData,
                    action: ACTION_ADD_DEPARTMENT
                })
                setActive(true);
                break;
            case KEY_ADD_ASSISTANT:
                toggleNodeDrawer({
                    type: DRAWER_ASSISTANT,
                    node: nodeData,
                    action: ACTION_ADD_ASSISTANT
                })
                setActive(true);
                break;
            case KEY_EDIT_ROLE:
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: nodeData,
                    action: ACTION_EDIT_ROLE
                })
                setActive(true);
                break;
            default:
                break;
        }
    };

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <div ref={!disabledDnD ? drag : undefined} className={styles['drag-node-container']}>
                <div
                    className={cx(styles['left-bar-node'], {
                        [styles['sidebar-dragging']]: isOverLeftBar
                    })}
                    ref={!disabledDnD ? dropToLeft : undefined}
                />
                <div ref={!disabledDnD ? drop : undefined}>
                    <Card
                        size="small"
                        className={cx('node-role-container', { 'node-role-container-hover': isHover || isOver || active })}
                        onMouseEnter={onMouseEnter}
                        onMouseLeave={onMouseLeave}
                        style={{ opacity: isOpacity ? '0.4' : '' }}
                    >
                        <div className={styles['title']}>
                            <div>
                                {nodeData.name}
                            </div>
                            <div className={cx(styles['toolbar'], {
                                [styles['toolbar-show']]: true
                            })}>
                                <MyLocationIcon className={styles['icon-btn']} onClick={onMyLocationClick} />
                                <Popover
                                    content={<MenuToolbar onMenuClick={onMenuClick(MENU_TOOLBAR)} />}
                                    trigger="click"
                                    onClick={onPopoverClick}
                                    onVisibleChange={onVisiblePopoverChange(MENU_TOOLBAR)}
                                    placement="bottom"
                                    overlayClassName="popover-node"
                                    visible={menuVisible === MENU_TOOLBAR}
                                >
                                    <MoreHorizIcon className={styles['icon-btn']} />

                                </Popover>
                            </div>
                        </div>
                        {user && (
                            <div className={styles['info']}>
                                <img className={styles['info-avatar']} src={user.avatar} alt="info-avatar" />
                                <div className={styles['info-name']}>
                                    {user.name}
                                </div>
                            </div>
                        )}
                        {numberChildOfNode > 0 && (
                            <div className={styles['extra']}>
                                <Tooltip
                                    placement="top"
                                    title={(
                                        <TooltipCollapseBtn
                                            numberChildOfNode={numberChildOfNode}
                                            numberDepartment={numberDepartment}
                                        />
                                    )
                                }>
                                    <Button className={styles['extra-btn']} onClick={onToggleCollapseClick}>
                                        <span>
                                            {`${numberChildOfNode}/${numberDepartment}`}
                                        </span>
                                        {!isCollapsed ? (
                                            <ExpandLessIcon className={styles['collapse-btn-icon']} />
                                        ) : (
                                                <ExpandMoreIcon className={styles['collapse-btn-icon']} />
                                            )}
                                    </Button>
                                </Tooltip>
                            </div>
                        )}
                        {isHover && (
                            <div className={styles['add-inferior-container']}>
                                <Popover
                                    content={<MenuAddInferior onMenuClick={onMenuClick(MENU_INFERIOR)} />}
                                    trigger="click"
                                    onClick={onPopoverClick}
                                    onVisibleChange={onVisiblePopoverChange(MENU_INFERIOR)}
                                    placement="bottom"
                                    overlayClassName="popover-node"
                                    visible={menuVisible === MENU_INFERIOR}
                                >
                                    <Button className={styles['add-btn']}>
                                        <AddIcon className={styles['add-icon']} />
                                    </Button>
                                </Popover>
                            </div>
                        )}
                    </Card>
                </div>
                <div
                    className={cx(styles['right-bar-node'], {
                        [styles['sidebar-dragging']]: isOverRightBar
                    })}
                    ref={!disabledDnD ? dropToRight : undefined}
                />
            </div>
        </>
    )
}

const TooltipCollapseBtn = ({ numberChildOfNode, numberDepartment }) => {
    return (
        <Col>
            <Row className={styles['tooltip-row']} type="flex" align="middle">
                <WorkOutlineIcon />
                <span>
                    {`${numberChildOfNode} vị trí công việc`}
                </span>
            </Row>
            <Row className={styles['tooltip-row']} type="flex" align="middle">
                <WorkOutlineIcon />
                <span>
                    {`${numberDepartment} phòng ban`}
                </span>
            </Row>
        </Col>
    )
}

export default RoleNode;
