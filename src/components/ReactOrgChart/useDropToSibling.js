import { useDrop } from 'react-dnd';

import {
    ORG_CHART_NODE_TYPE,
    TYPE_USER,
    TYPE_ROLE,
    ORG_CHART_NODE_EXIST_TYPE
} from '../../variables/orgChart';

const useDropToSibling = ({
    listAcceptType,
    listReceiveType,
    addSiblingNodeSaga,
    nodeData,
    direction,
    changeNodePositionToSiblingSaga
}) => {
    const [{ isOverSibling }, dropToSibling] = useDrop({
        accept: listAcceptType,
        drop: (item, monitor) => {
            const { type, nodeType } = item;
            if (type === ORG_CHART_NODE_TYPE && listReceiveType.includes(nodeType)) {
                // add new sibling node
                addSiblingNodeSaga({
                    item: {
                        ...item,
                        nodeType: nodeType === TYPE_USER ? TYPE_ROLE : nodeType
                    },
                    root: nodeData,
                    direction
                })
            }
            if (type === ORG_CHART_NODE_EXIST_TYPE && listReceiveType.includes(item.nodeType)) {
                // change position of node
                changeNodePositionToSiblingSaga({ item, root: nodeData, direction });
            }

        },
        collect: (monitor) => {
            return ({
                isOverSibling: monitor.isOver()
            })
        }
    });
    return {
        isOverSibling,
        dropToSibling
    }
};

export default useDropToSibling;
