import React, { useState } from 'react';
import { Card } from 'antd';

import cx from 'classnames';
import './styles.scss';
import styles from './styles.module.scss';

const MultiRoleNode = ({ nodeData, toggleListChildDrawer }) => {
    const { numOfChild } = nodeData;
    const [active, setActive] = useState(false);

    const onCardClick = () => {
        setActive(!active);
        toggleListChildDrawer({ node: nodeData });
    };

    const onMouseEnter = () => {
        setActive(false);
    };

    return (
        <div className={styles['drag-node-container']}>
            <div
                className={styles['left-bar-node']}
            />
            <Card
                size="small"
                className={cx('node-role-container', { 'node-role-container-hover': active })}
                onClick={onCardClick}
                onMouseEnter={onMouseEnter}
            >
                <div className={styles['multi-user-title']}>
                    Xem thêm cấp dưới trực tiếp
                </div>
                <div className={styles['multi-user-info']}>
                    {`(+${numOfChild} thành viên)`}
                </div>
            </Card>
            <div
                className={styles['right-bar-node']}
            />
        </div>
    )
};

export default MultiRoleNode;
