import React, { useState, useEffect, useCallback } from 'react';
import { Card, Button, Popover, Menu, Row } from 'antd';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { get } from 'lodash';
import cx from 'classnames';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import { useDrop, useDrag, DragPreviewImage } from 'react-dnd';

import {
    ORG_CHART_NODE_TYPE,
    TYPE_USER,
    ORG_CHART_NODE_EXIST_TYPE,
    KEY_DELETE_ASSISTANT,
    KEY_EDIT_ASSISTANT,
    DRAWER_ASSISTANT,
    ACTION_EDIT_ASSISTANT,
    TYPE_ROLE,
    TYPE_DEPARTMENT,
    TYPE_ASSISTANT,
    LEFT,
    RIGHT
} from '../../variables/orgChart';
import dragPreview from '../../fakeData/drag-preview.svg';
import styles from './styles.module.scss';
import './styles.scss';
import useDropToSibling from './useDropToSibling';

const listTypeAccept = [TYPE_USER];
const listTypeSiblingAccept = [TYPE_ROLE, TYPE_USER, TYPE_DEPARTMENT, TYPE_ASSISTANT];

const MenuToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_EDIT_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa trợ lý</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_ASSISTANT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá trợ lý</div>
            </Menu.Item>
        </Menu>
    )
}

const AssistantNode = ({
    nodeData = {},
    addUserToNodeSaga,
    changeNodePositionToChildrenSaga,
    setNodeDraggingId,
    listNodeDraggingId,
    toggleNodeDrawer,
    addSiblingNodeSaga,
    changeNodePositionToSiblingSaga,
    disabledDnD
}) => {
    const [isHover, setIsHover] = useState(false);
    const [alwaysHover, setAlwaysHover] = useState(false);
    const [menuToolbarVisible, setMenuToolbarVisible] = useState(false);
    const [active, setActive] = useState(false);

    const [{ canDrop, isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        drop: (item, monitor) => {
            const { type } = item;
            if (type === ORG_CHART_NODE_TYPE && listTypeAccept.includes(item.nodeType)) {
                // cannot add node to assistant node
                if (item.nodeType === TYPE_USER) {
                    return addUserToNodeSaga({ item, root: nodeData });
                }
            }
            if (type === ORG_CHART_NODE_EXIST_TYPE && listTypeAccept.includes(item.nodeType)) {
                // change position of node
                changeNodePositionToChildrenSaga({ item, root: nodeData });
            }
        },
        collect: (monitor) => ({
            isOver: monitor.isOver()
        })
    })

    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: ORG_CHART_NODE_EXIST_TYPE,
            nodeType: nodeData.type,
            nodeData
        },
        end: (item, monitor) => {
            setNodeDraggingId();
        },
        collect: (monitor) => {
            const isDragging = monitor.isDragging();
            if (isDragging) {
                setNodeDraggingId(nodeData)
            }
            return ({
                isDragging
            })
        }
    })

    const {
        isOverSibling: isOverLeftBar,
        dropToSibling: dropToLeft
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: LEFT
    });

    const {
        isOverSibling: isOverRightBar,
        dropToSibling: dropToRight
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: RIGHT
    });

    const onMouseEnter = () => {
        setIsHover(true);
        setActive(false);
    };

    const onMouseLeave = () => {
        if (!alwaysHover) {
            setIsHover(false);
        }
    };

    const { user } = nodeData;

    const onPopoverClick = () => {
        setAlwaysHover(!alwaysHover)
    };

    const onVisiblePopoverChange = (visible) => {
        if (!visible) {
            // when close popover, turn off always hover
            setAlwaysHover(false);
            setIsHover(false);
        }
        setMenuToolbarVisible(visible);
    };

    useEffect(() => {
        if (isDragging) {
            setIsHover(false);
        }
    }, [isDragging]);

    const isOpacity = listNodeDraggingId.includes(nodeData.id);

    const onMenuClick = ({ item, key }) => {
        setMenuToolbarVisible(false);
        setAlwaysHover(false);
        switch (key) {
            case KEY_EDIT_ASSISTANT:
                toggleNodeDrawer({
                    type: DRAWER_ASSISTANT,
                    node: nodeData,
                    action: ACTION_EDIT_ASSISTANT
                })
                setActive(true);
                break;
            default:
                break;
        }
    };

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <div ref={!disabledDnD ? drag : undefined} className={styles['drag-node-container']}>
                <div
                    className={cx(styles['left-bar-node'], {
                        [styles['sidebar-dragging']]: isOverLeftBar
                    })}
                    ref={!disabledDnD ? dropToLeft : undefined}
                />
                <div ref={!disabledDnD ? drop : undefined}>
                    <Card
                        size="small"
                        className={cx("node-role-container", "assistant-role-container", { 'node-role-container-hover': isHover || isOver || active })}
                        onMouseEnter={onMouseEnter}
                        onMouseLeave={onMouseLeave}
                        style={{ opacity: isOpacity ? '0.4' : '' }}
                    >
                        <div className={styles['title']}>
                            <Row type="flex" align="middle" className={styles['assistant-title']}>
                                <SupervisedUserCircleIcon className={styles['assistant-icon']} />
                                {nodeData.name}
                                <div className={cx(styles['toolbar'], {
                                    [styles['toolbar-show']]: true
                                })}>
                                    <Popover
                                        content={<MenuToolbar onMenuClick={onMenuClick} />}
                                        trigger="click"
                                        onClick={onPopoverClick}
                                        onVisibleChange={onVisiblePopoverChange}
                                        placement="bottom"
                                        overlayClassName="popover-node"
                                        visible={menuToolbarVisible}
                                    >
                                        <MoreHorizIcon className={styles['icon-btn']} />
                                    </Popover>
                                </div>
                            </Row>
                            {/* {isHover && (
                                <div className={cx(styles['toolbar'], {
                                    [styles['toolbar-show']]: isHover
                                })}>
                                    <Popover
                                        content={<MenuToolbar onMenuClick={onMenuClick} />}
                                        trigger="click"
                                        onClick={onPopoverClick}
                                        onVisibleChange={onVisiblePopoverChange}
                                        placement="bottom"
                                        overlayClassName="popover-node"
                                        visible={menuToolbarVisible}
                                    >
                                        <MoreHorizIcon className={styles['icon-btn']} />
                                    </Popover>
                                </div>
                            )} */}
                        </div>
                        {user && (
                            <div className={cx(styles['info'], styles['assistant-info'])}>
                                <img className={styles['info-avatar']} src={user.avatar} alt="info-avatar" />
                                <div className={styles['info-name']}>
                                    {user.name}
                                </div>
                            </div>
                        )}
                    </Card>
                </div>
                <div
                    className={cx(styles['right-bar-node'], {
                        [styles['sidebar-dragging']]: isOverRightBar
                    })}
                    ref={!disabledDnD ? dropToRight : undefined}
                />
            </div>
        </>
    )
}

export default AssistantNode;
