import React from "react";
import { useDrop } from 'react-dnd';

import OrganizationChart from "./libs/ChartContainer";
import NodeContainer from "./NodeContainer";
import {
    ORG_CHART_NODE_TYPE,
    TYPE_ROLE,
    TYPE_ASSISTANT,
    TYPE_DEPARTMENT,
    ORG_CHART_NODE_EXIST_TYPE,
    TYPE_USER
} from '../../variables/orgChart';
import styles from './styles.module.scss';
import './styles.scss';

const listTypeAccept = [TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT, TYPE_USER];

const ReactOrgChart = ({
    onWheel,
    percentChart,
    zoominLimit,
    zoomoutLimit,
    listNodeCollapse,
    datasource,
    nodeProps,
    listNodeDraggingId,
    rootNode,
    listNodeData,
    addChildNodeSaga,
    changeNodePositionToChildrenSaga
}) => {
    const [{ canDrop, isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        drop: (item, monitor) => {
            const { type } = item;
            const isDropToNode = monitor.didDrop();
            if (!isDropToNode) {
                if (type === ORG_CHART_NODE_TYPE && listTypeAccept.includes(item.nodeType)) {
                    // add new node
                    addChildNodeSaga({ item });
                }
                if (type === ORG_CHART_NODE_EXIST_TYPE) {
                    // change position of node
                    changeNodePositionToChildrenSaga({ item });
                }
            }
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop()
        })
    })

    return (
        <div ref={drop} className={styles['react-org-chart-container']}>
            <OrganizationChart
                datasource={datasource}
                chartClass="myChart"
                NodeTemplate={NodeContainer}
                collapsible={false}
                pan={true}
                zoom={true}
                zoominLimit={zoominLimit}
                zoomoutLimit={zoomoutLimit}
                onWheel={onWheel}
                percentChart={percentChart}
                listNodeCollapse={listNodeCollapse}
                nodeProps={nodeProps}
                draggable={true}
                listNodeDraggingId={listNodeDraggingId}
                rootNode={rootNode}
            />
        </div>

    )
};

export default ReactOrgChart;
