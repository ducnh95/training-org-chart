import React, { useState, useEffect, useCallback } from 'react';
import { Card, Button, Popover, Menu, Row, Col } from 'antd';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import AddIcon from '@material-ui/icons/Add';
import { get, isEmpty } from 'lodash';
import cx from 'classnames';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import { useDrop, useDrag, DragPreviewImage } from 'react-dnd';

import {
    TYPE_ROLE,
    TYPE_DEPARTMENT,
    ORG_CHART_NODE_TYPE,
    ORG_CHART_NODE_EXIST_TYPE,
    TYPE_USER,
    KEY_ADD_ROLE,
    KEY_CHANGE_DIRECT_PARENT,
    KEY_EDIT_DEPARTMENT,
    KEY_DELETE_DEPARTMENT,
    ACTION_EDIT_DEPARTMENT,
    DRAWER_ROLE,
    DRAWER_DEPARTMENT,
    ACTION_ADD_ROLE,
    MENU_TOOLBAR,
    MENU_INFERIOR,
    TYPE_ASSISTANT,
    LEFT,
    RIGHT
} from '../../variables/orgChart';
import dragPreview from '../../fakeData/drag-preview.svg';
import useDropToSibling from './useDropToSibling';
import styles from './styles.module.scss';
import './styles.scss';

const listTypeAccept = [TYPE_ROLE, TYPE_USER];
const listTypeSiblingAccept = [TYPE_ROLE, TYPE_USER, TYPE_DEPARTMENT, TYPE_ASSISTANT];

const MenuAddInferior = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_ADD_ROLE} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Thêm vị trí công việc</div>
            </Menu.Item>
        </Menu>
    )
};

const MenuToolbar = ({ onMenuClick }) => {
    return (
        <Menu
            className={styles['menu-add']}
            onClick={onMenuClick}
        >
            <Menu.Item key={KEY_CHANGE_DIRECT_PARENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Đổi quản lý trực tiếp</div>
            </Menu.Item>
            <Menu.Item key={KEY_EDIT_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Sửa phòng ban</div>
            </Menu.Item>
            <Menu.Item key={KEY_DELETE_DEPARTMENT} className={styles['menu-item-container']}>
                <WorkOutlineIcon />
                <div className={styles['menu-item-content']}>Xoá phòng ban</div>
            </Menu.Item>
        </Menu>
    )
}


const DepartmentNode = ({
    nodeData = {},
    addChildNodeSaga,
    changeNodePositionToChildrenSaga,
    setNodeDraggingId,
    listNodeDraggingId,
    toggleNodeDrawer,
    addSiblingNodeSaga,
    changeNodePositionToSiblingSaga,
    numberChildOfNode,
    disabledDnD
}) => {
    const [isHover, setIsHover] = useState(false);
    const [alwaysHover, setAlwaysHover] = useState(false);
    const [active, setActive] = useState(false);
    const [menuVisible, setMenuVisible] = useState(); // type of menu visible


    const [{ canDrop, isOver }, drop] = useDrop({
        accept: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        drop: (item, monitor) => {
            const { type } = item;
            if (type === ORG_CHART_NODE_TYPE && listTypeAccept.includes(item.nodeType)) {
                // add new node
                if (item.nodeType === TYPE_USER) {
                    // add node and add user to that node
                    return addChildNodeSaga({
                        item: {
                            ...item,
                            nodeType: TYPE_ROLE
                        },
                        root: nodeData,
                    });
                }
                addChildNodeSaga({
                    item,
                    root: nodeData
                });
            }
            if (type === ORG_CHART_NODE_EXIST_TYPE && listTypeAccept.includes(item.nodeType)) {
                // change position of node
                changeNodePositionToChildrenSaga({ item, root: nodeData });
            }

        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop()
        })
    })

    const [{ isDragging }, drag, preview] = useDrag({
        item: {
            type: ORG_CHART_NODE_EXIST_TYPE,
            nodeType: nodeData.type,
            nodeData
        },
        end: (item, monitor) => {
            setNodeDraggingId();
        },
        collect: (monitor) => {
            const isDragging = monitor.isDragging();
            if (isDragging) {
                setNodeDraggingId(nodeData)
            }
            return ({
                isDragging
            })
        }
    })

    const {
        isOverSibling: isOverLeftBar,
        dropToSibling: dropToLeft
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: LEFT
    });

    const {
        isOverSibling: isOverRightBar,
        dropToSibling: dropToRight
    } = useDropToSibling({
        listAcceptType: [ORG_CHART_NODE_TYPE, ORG_CHART_NODE_EXIST_TYPE],
        listReceiveType: listTypeSiblingAccept,
        addSiblingNodeSaga,
        changeNodePositionToSiblingSaga,
        nodeData,
        direction: RIGHT
    });

    const onMouseEnter = useCallback(() => {
        setIsHover(true);
        setActive(false);
    }, []);

    const onMouseLeave = useCallback(() => {
        if (!alwaysHover) {
            setIsHover(false);
        }
    }, [alwaysHover]);

    const onPopoverClick = useCallback(() => {
        setAlwaysHover(!alwaysHover)
    }, [alwaysHover]);

    const onVisiblePopoverChange = useCallback((menuType) => (visible) => {
        if (!visible) {
            // when close popover, turn off always hover
            setAlwaysHover(false);
            setIsHover(false);
            setMenuVisible();
        }
        if (visible) {
            setMenuVisible(menuType);
        }
    }, []);

    useEffect(() => {
        if (isDragging) {
            setIsHover(false);
        }
    }, [isDragging])

    const onMenuClick = useCallback((typeMenu) => ({ item, key }) => {
        setMenuVisible();
        setAlwaysHover(false);
        switch (key) {
            case KEY_ADD_ROLE:
                toggleNodeDrawer({
                    type: DRAWER_ROLE,
                    node: nodeData,
                    action: ACTION_ADD_ROLE
                })
                setActive(true);
                break;
            case KEY_EDIT_DEPARTMENT:
                toggleNodeDrawer({
                    type: DRAWER_DEPARTMENT,
                    node: nodeData,
                    action: ACTION_EDIT_DEPARTMENT
                })
                setActive(true);
                break;
            default:
                break;
        }
    }, [toggleNodeDrawer, nodeData]);

    const isOpacity = listNodeDraggingId.includes(nodeData.id);
    const departmentName = get(nodeData, 'department.name', '');
    const departmentId = get(nodeData, 'department.id', '');

    return (
        <>
            <DragPreviewImage connect={preview} src={dragPreview} />
            <div ref={!disabledDnD ? drag : undefined} className={styles['drag-node-container']}>
                <div
                    className={cx(styles['left-bar-node'], {
                        [styles['sidebar-dragging']]: isOverLeftBar
                    })}
                    ref={!disabledDnD ? dropToLeft : undefined}
                />
                <div ref={!disabledDnD ? drop : undefined}>
                    <Card
                        size="small"
                        className={cx("node-role-container", "department-role-container", { 'node-role-container-hover': isHover || isOver || active })}
                        onMouseEnter={onMouseEnter}
                        onMouseLeave={onMouseLeave}
                        style={{ opacity: isOpacity ? '0.4' : '' }}
                    >
                        <Row type="flex" justify="space-between" align="top" className={styles['department-title']}>
                            <Row type="flex">
                                <WorkOutlineIcon />
                                <div className="align-items-start d-flex flex-column ml-3">
                                    <div className={styles['department-name']}>
                                        {departmentName}
                                    </div>
                                    <div className={styles['department-id']}>
                                        {departmentId}
                                    </div>
                                </div>

                            </Row>
                            <div>
                                <Popover
                                    content={<MenuToolbar onMenuClick={onMenuClick(MENU_TOOLBAR)} />}
                                    trigger="click"
                                    onClick={onPopoverClick}
                                    onVisibleChange={onVisiblePopoverChange(MENU_TOOLBAR)}
                                    placement="bottom"
                                    overlayClassName="popover-node"
                                    visible={menuVisible === MENU_TOOLBAR}
                                >
                                    <MoreHorizIcon
                                        className="icon-btn"
                                    />
                                </Popover>
                                <Button className={cx(styles['extra-btn'], 'm-0')}>
                                        {numberChildOfNode}
                                </Button>
                            </div>
                        </Row>
                        {isHover && (
                            <div className={styles['add-inferior-container']}>
                                <Popover
                                    content={<MenuAddInferior onMenuClick={onMenuClick(MENU_INFERIOR)} />}
                                    trigger="click"
                                    onClick={onPopoverClick}
                                    onVisibleChange={onVisiblePopoverChange(MENU_INFERIOR)}
                                    placement="bottom"
                                    overlayClassName="popover-node"
                                    visible={(menuVisible === MENU_INFERIOR)}
                                >
                                    <Button className={styles['add-btn']}>
                                        <AddIcon className={styles['add-icon']} />
                                    </Button>
                                </Popover>
                            </div>
                        )}
                    </Card>
                </div>
                <div
                    className={cx(styles['right-bar-node'], {
                        [styles['sidebar-dragging']]: isOverRightBar
                    })}
                    ref={!disabledDnD ? dropToRight : undefined}
                />
            </div>
        </>
    )
};

export default DepartmentNode;
