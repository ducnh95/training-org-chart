import {
    getDepartmentId
} from '../utils';

export const TYPE_ROLE = 'TYPE_ROLE';
export const TYPE_ASSISTANT = 'TYPE_ASSISTANT';
export const TYPE_DEPARTMENT = 'TYPE_DEPARTMENT';
export const TYPE_USER = 'TYPE_USER';
export const TYPE_MULTI_USER = 'TYPE_MULTI_USER';

export const ORG_CHART_NODE_EXIST_TYPE = 'ORG_CHART_NODE_EXIST_TYPE';
export const ORG_CHART_NODE_TYPE = 'ORG_CHART_NODE_TYPE';

// DRAWER TYPE

export const DRAWER_ROLE = 'DRAWER_ROLE';
export const DRAWER_DEPARTMENT = 'DRAWER_DEPARTMENT';
export const DRAWER_ASSISTANT = 'DRAWER_ASSISTANT';

// KEY FOR MENU
export const KEY_ADD_ASSISTANT = 'KEY_ADD_ASSISTANT';
export const KEY_EDIT_ASSISTANT = 'KEY_EDIT_ASSISTANT';
export const KEY_DELETE_ASSISTANT = 'KEY_DELETE_ASSISTANT';
export const KEY_ADD_ROLE = 'KEY_ADD_ROLE';
export const KEY_EDIT_ROLE = 'KEY_EDIT_ROLE';
export const KEY_DELETE_ROLE = 'KEY_DELETE_ROLE';
export const KEY_ADD_DEPARTMENT = 'KEY_ADD_DEPARTMENT';
export const KEY_EDIT_DEPARTMENT = 'KEY_EDIT_DEPARTMENT';
export const KEY_DELETE_DEPARTMENT = 'KEY_DELETE_DEPARTMENT';
export const KEY_CHANGE_DIRECT_PARENT = 'KEY_CHANGE_DIRECT_PARENT';
export const KEY_DELETE_USER = 'KEY_DELETE_USER';

// ACTION FOR MENU TO DRAWER
export const ACTION_ADD_ROLE = 'ACTION_ADD_ROLE';
export const ACTION_EDIT_ROLE = 'ACTION_EDIT_ROLE';
export const ACTION_ADD_ASSISTANT = 'ACTION_ADD_ASSISTANT';
export const ACTION_EDIT_ASSISTANT = 'ACTION_EDIT_ASSISTANT';
export const ACTION_ADD_DEPARTMENT = 'ACTION_ADD_DEPARTMENT';
export const ACTION_EDIT_DEPARTMENT = 'ACTION_EDIT_DEPARTMENT';

// TYPE MENU
export const MENU_SUPERIOR = 'MENU_SUPERIOR';
export const MENU_INFERIOR = 'MENU_INFERIOR';
export const MENU_TOOLBAR = 'MENU_TOOLBAR';

// DIRECTION
export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';

// SETTING
export const DEFAULT_NUMBER_CHILDREN_RENDER_ALL = 5;
export const DEFAULT_NUMBER_LAYER_SHOW_ALL = 5;

// ZOOM IN ZOOM OUT
export const PERCENT_CHART_AMOUNT_CHANGE = 25;
export const PERCENT_CHART_DEFAULT = 100;
export const PERCENT_CHART_MIN = 20;
export const PERCENT_CHART_MAX = 200;
export const ZOOMIN_LIMIT = PERCENT_CHART_MAX / 100;
export const ZOOMOUT_LIMIT = PERCENT_CHART_MIN / 100;
export const WHEEL_AMOUNT_CHANGE = 10;

// COLLAPSE
export const DEFAULT_LAYER_COLLAPSE = 2;

// DEFAULT NODE CREATE
export const DEFAULT_ROLE_NAME = 'Tên công việc';
export const DEFAULT_ASSISTANT_NAME = 'Vị trí trợ lý';
export const DEFAULT_DEPARTMENT_NAME = 'Phòng ban';
export const DEFAULT_DEPARTMENT_ID = 'Mã phòng ban';
export const DEFAULT_DEPARTMENT_DESCRIPTION = '';

// DEFAULT DIRECTION ADD NODE
export const DEFAULT_DIRECTION_ADD_NODE = RIGHT;
export const DEFAULT_DIRECTION_CHANGE_NODE = RIGHT;

// LIST CHILD DRAWER TYPE DRAGGING
export const CHILD_DRAWER_DEPARTMENT = 'CHILD_DRAWER_DEPARTMENT';
export const CHILD_DRAWER_NODE = 'CHILD_DRAWER_NODE';