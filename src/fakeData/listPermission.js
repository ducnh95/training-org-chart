const listPermission = [
    {
        id: 'pm1',
        name: 'Permission1'
    },
    {
        id: 'pm2',
        name: 'Permission2'
    },
    {
        id: 'pm3',
        name: 'Permission3'
    },
    {
        id: 'pm4',
        name: 'Permission4'
    },
    {
        id: 'pm5',
        name: 'Permission5'
    },
    {
        id: 'pm6',
        name: 'Permission6'
    }
];

export default listPermission;
