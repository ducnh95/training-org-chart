const listDepartment = [
    {
        id: '1',
        name: 'Phong 1',
        description: 'Chịu trách nhiệm tối ưu hoá chi phí và tăng tốc quy trình. Nhiệm vụ chính là thiết kế các thông số kĩ thuật phù hợp với các yêu cầu và các vấn đề kinh doanh.'
    },
    {
        id: '2',
        name: 'Phong 2',
        description: 'Chịu trách nhiệm tối ưu hoá chi phí và tăng tốc quy trình. Nhiệm vụ chính là thiết kế các thông số kĩ thuật phù hợp với các yêu cầu và các vấn đề kinh doanh.'
    },
    {
        id: '3',
        name: 'Ten 3',
        description: 'Khong co description'
    },
    {
        id: '4',
        name: 'Ban ke hoach',
        description: 'Day la ban ke hoach'
    },
];

export default listDepartment;
