import avatar from './avatar';
import { TYPE_ROLE, TYPE_ASSISTANT, TYPE_DEPARTMENT } from '../variables/orgChart';

export const listNodeData = {};

export const emptyChartData = {
    id: null,
    children: []
};

export const listNodeDataTest = {
    id: null,
    children: {
        'n0': {
            id: "n0",
            name: "n0",
            order: 0,
            parentId: null,
            parent: null,
            user: {
                id: 'nv1',
                name: 'Nhân viên 1',
                email: 'nv1@gmail.com',
                avatar,
                accessPermissions: {
                    id: 'pm1',
                    name: 'Permission1'
                }
            },
            type: TYPE_ROLE,
            children: {
                'n01': {
                    id: "n01",
                    parentId: 'n0',
                    name: "n01",
                    order: 0,
                    type: TYPE_ROLE,
                    parent: ['n0'],
                    user: {
                        id: 'nv2',
                        name: 'Nhân viên 2',
                        email: 'nv2@gmail.com',
                        avatar,
                        accessPermissions: {
                            id: 'pm2',
                            name: 'Permission2'
                        }
                    },
                    children: {
                        'n011': {
                            id: 'n011',
                            order: 0,
                            parentId: 'n01',
                            parent: ['n0', 'n01'],
                            type: TYPE_ASSISTANT,
                            name: 'n011',
                            children: {}
                        },
                        'n012': {
                            id: 'n012',
                            order: 1,
                            parentId: 'n01',
                            parent: ['n0', 'n01'],
                            type: TYPE_ROLE,
                            name: 'n012',
                            children: {
                                'n0121': {
                                    id: 'n0121',
                                    order: 0,
                                    parentId: 'n012',
                                    parent: ['n0', 'n01', 'n012'],
                                    type: TYPE_ROLE,
                                    name: 'n0121',
                                    children: {}
                                },
                                'n0122': {
                                    id: 'n0122',
                                    order: 1,
                                    parentId: 'n012',
                                    parent: ['n0', 'n01', 'n012'],
                                    type: TYPE_ROLE,
                                    name: 'n0122',
                                    children: {}
                                }
                            }
                        }
                    }
                },
                'n02': {
                    id: "n02",
                    parentId: 'n0',
                    order: 1,
                    parent: ['n0'],
                    type: TYPE_DEPARTMENT,
                    children: {},
                    department: {
                        id: 'TK001',
                        name: 'Ban kiểm toán',
                        description: 'Chịu trách nhiệm tối ưu hoá chi phí và tăng tốc quy trình. Nhiệm vụ chính là thiết kế các thông số kĩ thuật phù hợp với các yêu cầu và các vấn đề kinh doanh.'
                    }
                }
            }
        }

    }
}

export const listUserAddedDefault = [
    {
        id: 'nv1',
        name: 'Nhân viên 1',
        email: 'nv1@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm1',
            name: 'Permission1'
        }
    },
    {
        id: 'nv2',
        name: 'Nhân viên 2',
        email: 'nv2@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm2',
            name: 'Permission2'
        }
    }
]

export default listNodeData;