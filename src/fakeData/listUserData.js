import avatar from './avatar';

const listUserData = [
    {
        id: 'nv1',
        name: 'Nhân viên 1',
        email: 'nv1@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm1',
            name: 'Permission1'
        },
        role: 'Developer'
    },
    {
        id: 'nv2',
        name: 'Nhân viên 2',
        email: 'nv2@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm2',
            name: 'Permission2'
        }
    },
    {
        id: 'nv3',
        name: 'Nhân viên 3',
        email: 'nv3@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm3',
            name: 'Permission3'
        },
        role: 'Developer'
    },
    {
        id: 'nv4',
        name: 'Nhân viên 4',
        email: 'nv4@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm4',
            name: 'Permission4'
        },
        role: 'Tester'
    },
    {
        id: 'nv5',
        name: 'Nhân viên 5',
        email: 'nv5@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv6',
        name: 'Nhân viên 6',
        email: 'nv6@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm6',
            name: 'Permission6'
        }
    },
    {
        id: 'nv7',
        name: 'Nhân viên 7',
        email: 'nv7@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv8',
        name: 'Nhân viên 8',
        email: 'nv8@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv9',
        name: 'Nhân viên 9',
        email: 'nv9@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv10',
        name: 'Nhân viên 10',
        email: 'nv10@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv11',
        name: 'Nhân viên 11',
        email: 'nv11@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv12',
        name: 'Nhân viên 12',
        email: 'nv12@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    },
    {
        id: 'nv13',
        name: 'Nhân viên 13',
        email: 'nv13@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        },
        role: 'QA'
    },
    {
        id: 'nv14',
        name: 'Nhân viên 14',
        email: 'nv14@gmail.com',
        avatar,
        accessPermissions: {
            id: 'pm5',
            name: 'Permission5'
        }
    }
];

export default listUserData;