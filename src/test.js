const orgNode = [
    {
        id: '1',
        order: 1, 
        position : {
            id : "po01",
            name : "test 1",
            is_assistant : true
        },
        employee: {
            name: 'Duc',
            id: 'emp1',
            avatar: 'avatar'
        },
        department : {
            id : "dep01",
            name : "test 1",
            code: 'PB1',
        },
        manage_department : [
            {
                id : "dep01",
                name : "test 2",
                code: 'PB2'
            },
            {
                id : "dep03",
                name : "test 3",
                code: 'PB3'
            }
        ],
        parent_id : "01",
        parents: ['01']
    },
    {
        id: '01',
        order: 1,
        position : {
            id : "po01",
            name : "test 1",
            is_assistant : true
        },
        employee: {
            name: 'Duc',
            id: 'emp1',
            avatar: 'avatar'
        },
        department : {
            id : "dep01",
            name : "test 1",
            code: 'PB1'
        },
        manage_department : [
            {
                id : "dep02",
                name : "test 2",
                code: 'PB2'
            },
            {
                id : "dep03",
                name : "test 3",
                code: 'PB3'
            }
        ],
        parent_id : null,
        parents: null
    }
    
];


const orgNode2 = [
    {
        id: 'role1',
        order: 1,
        position: {
            id: 'role1',
            name: 'role1',
            is_assistant: false
        },
        employee: {
            name: 'emp1',
            id: 'emp1'
        },
        department: null,
        manage_department: [
            {
                id: 'dep1',
                name: 'dep1',
                code: 'dep1'
            },
            {
                id: 'dep2',
                name: 'dep2',
                code: 'dep2'
            }
        ],
        parent_id: null,
        parents: null,
        
    },
    {
        id: 'role2',
        order: 1,
        position: {
            id: 'role2',
            name: 'role2',
            is_assistant: false
        },
        employee: null,
        department: null,
        manage_department: null,
        parent_id: 'role1',
        parents: ['role1']
    },
    {
        id: 'role3',
        order: 1,
        position: {
            id: 'role3',
            name: 'role3',
            is_assistant: true
        },
        employee: null,
        department: null,
        manage_department: null,
        parent_id: 'role2',
        parents: ['role1', 'role2']
    },
    {
        id: 'role4',
        order: 2,
        position: {
            id: 'role4',
            name: 'role4',
            is_assistant: false
        },
        employee: null,
        department: {
            id: 'dep1',
            name: 'dep1',
            code: 'dep1'
        },
        manage_department: [
            {
                id: 'dep3',
                name: 'dep3',
                code: 'dep3'
            }
        ],
        parent_id: 'role1',
        parents: ['role1']
    },
    {
        id: 'role5',
        order: 3,
        position: {
            id: 'role5',
            name: 'role5',
            is_assistant: false
        },
        employee: null,
        department: {
            id: 'dep2',
            name: 'dep2',
            code: 'dep2'
        },
        manage_department: [
            {
                id: 'dep6',
                name: 'dep6',
                code: 'dep6'
            }
        ],
        parent_id: 'role1',
        parents: ['role1']
    },
    {
        id: 'role6',
        order: 4,
        position: {
            id: 'role6',
            name: 'role6',
            is_assistant: false
        },
        employee: null,
        department: {
            id: 'dep2',
            name: 'dep2',
            code: 'dep2'
        },
        manage_department: null,
        parent_id: 'role1',
        parents: ['role1']
    }
]


module.exports = orgNode2;

// export default orgNode;