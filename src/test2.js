// import { get, isEmpty } from 'lodash';
// var lodash = require('lodash');
var { get, isEmpty, toArray } = require('lodash')
var orgNode = require('./test')
// var { TYPE_ASSISTANT, TYPE_ROLE, TYPE_DEPARTMENT } = require('./variables/orgChart');
// import orgNode from './test';
// import { TYPE_ASSISTANT, TYPE_ROLE, TYPE_DEPARTMENT } from './variables/orgChart';
var fs = require('fs');

let dataChart = {};
dataChart = {
    id: null,
    children: {}
};


var listNodeResTest = {
    id: null,
    children: {
        'n0': {
            id: "n0",
            name: "n0",
            order: 0,
            parentId: null,
            parents: null,
            user: {
                id: 'nv1',
            },
            children: {
                'n01': {
                    id: 'n01',
                    parentId: 'n0',
                    parents: ['n0'],
                    order: 0,
                    children: {}
                }
            }
        }
    }
};


let listNode = {}

orgNode.forEach(node => {
    var { id } = node;
    if (!listNode[id]) {
        var newNode = {};
        var { position, employee, department, manage_department, parent_id } = node;
        var parents = node.parents || [];
        newNode.id = node.id;
        newNode.order = node.order;
        newNode.name = position.name;
        newNode.employee = employee;
        newNode.parentId = parent_id;
        newNode.parents = parents;
        if (position.is_assistant) {
            newNode.type = 'TYPE_ASSISTANT'
        } else {
            newNode.type = 'TYPE_ROLE'
        }
        if (!isEmpty(department)) {
            var departmentId = department.id;
            newNode.parentId = departmentId;
            newNode.parents.push(departmentId);
        }
        if (!isEmpty(manage_department)) {
            manage_department.forEach((item, index) => {
                if (!isEmpty(item)) {
                    var departmentId = item.id;
                    listNode[departmentId] = {
                        id: departmentId,
                        name: item.name,
                        department: item,
                        parentId: id,
                        parents: [...parents, id],
                        type: 'TYPE_DEPARTMENT',
                        order: index + 1
                    }
                }
            })
        }
        listNode[id] = newNode;
    }
})

function convertToTree(arr) {
    var tree = [],
        mappedArr = {},
        arrElem,
        mappedElem;

    // First map the nodes of the array to an object -> create a hash table.
    for(var i = 0, len = arr.length; i < len; i++) {
      arrElem = arr[i];
      mappedArr[arrElem.id] = arrElem;
      mappedArr[arrElem.id]['children'] = {};
    }


    for (var id in mappedArr) {
      if (mappedArr.hasOwnProperty(id)) {
        mappedElem = mappedArr[id];
        // If the element is not at the root level, add it to its parent array of children.
        if (mappedElem.parentId) {
          mappedArr[mappedElem['parentId']]['children'][mappedElem.id] = mappedElem;
        //   mappedArr[mappedElem['parentId']]['children'].push(mappedElem);
        }
        // If the element is at the root level, add it to first level elements array.
        else {
          tree.push(mappedElem);
        }
      }
    }
    return tree;
  }


var myTree = convertToTree(toArray(listNode));

fs.writeFile('result.json', JSON.stringify(myTree, null, 4), function (err) {
    if (err) throw err;
    console.log('Replaced!');
  });