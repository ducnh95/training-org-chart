import { all } from 'redux-saga/effects';
import orgChartSaga from './orgChart';

export default function* rootSaga() {
    yield all([
        orgChartSaga()
    ]);
};
