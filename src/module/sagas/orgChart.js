import { takeEvery, all, put, select } from 'redux-saga/effects';
import { isEmpty } from 'lodash';

import {
    ADD_CHILD_NODE_SAGA,
    ADD_SIBLING_NODE_SAGA,
    ADD_USER_TO_NODE_SAGA,
    CHANGE_NODE_POSITION_TO_CHILDREN_SAGA,
    CHANGE_NODE_POSITION_TO_SIBLING_SAGA,
    addChildNode,
    addUserToNode,
    addSiblingNode,
    changeNodePositionToChildren,
    changeNodePositionToSibling,
    toggleNodeDrawer,
    setRelativeNode,
    toggleListChildDrawer
} from '../actions/orgChart';
import {
    selectListNodeData,
    selectNumberChildrenRenderAll,
    selectNumberChildOfNode
} from '../selectors/orgChart';
import { TYPE_ROLE } from '../../variables/orgChart';

function* addChildNodeSaga(action) {
    const { item, root, direction, isChangeRelativeNode } = action.payload;

    // handle call API here
    yield put(addChildNode({
        item,
        root,
        direction
    }))
    if (isChangeRelativeNode) {
        yield put(setRelativeNode(root))
    }

    if (!isEmpty(root)) {
        const { type: rootType } = root;
        if (rootType === TYPE_ROLE) {
            const numberChildrenRenderAll = yield select(selectNumberChildrenRenderAll);
            const numberChildOfNode = yield select(selectNumberChildOfNode(root));
            if (numberChildOfNode > numberChildrenRenderAll) {
                yield put(toggleListChildDrawer({ root }));
            }
        }
    }
  
    yield put(toggleNodeDrawer());
}

function* addUserToNodeSaga(action) {
    const { item, root } = action.payload;
    // handle call API here
    yield put(addUserToNode({
        item,
        root
    }))
}

function* addSiblingNodeSaga(action) {
    const { item, root, direction } = action.payload;
    const parentRootId = root.parentId;
    if (!parentRootId) {
        // only support 1 root
        return;
    }
    // handle call API here
    yield put(addSiblingNode({
        item,
        root,
        direction
    }))
}

function* changeNodePositionToChildrenSaga(action) {
    const { item, root, direction } = action.payload;
    const listNodeData = yield select(selectListNodeData);
    if (!root && Object.keys(listNodeData.children).length === 1) {
        // only support 1 root in chart
        return;
    }
    // handle call API here
    yield put(changeNodePositionToChildren({
        item,
        root,
        direction
    }))
}

function* changeNodePositionToSiblingSaga(action) {
    const { item, root, direction } = action.payload;
    const rootParentId = root.parentId;
    if (!rootParentId) {
        // only support 1 root
        return;
    }
    // handle call API here
    yield put(changeNodePositionToSibling({
        item,
        root,
        direction
    }))
}

export default function* userSaga() {
    yield all([
        takeEvery(ADD_CHILD_NODE_SAGA, addChildNodeSaga),
        takeEvery(ADD_USER_TO_NODE_SAGA, addUserToNodeSaga),
        takeEvery(ADD_SIBLING_NODE_SAGA, addSiblingNodeSaga),
        takeEvery(CHANGE_NODE_POSITION_TO_CHILDREN_SAGA, changeNodePositionToChildrenSaga),
        takeEvery(CHANGE_NODE_POSITION_TO_SIBLING_SAGA, changeNodePositionToSiblingSaga)
    ]);
}