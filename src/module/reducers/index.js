import { combineReducers } from 'redux';
import orgChartReducer, { namespace as orgChartNamespace } from './orgChart';

export default combineReducers({
    [orgChartNamespace]: orgChartReducer
});
