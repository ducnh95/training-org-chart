import { cloneDeep, isEmpty } from 'lodash';

import { listNodeDataTest, listUserAddedDefault } from '../../fakeData/listNodeData';
import listUserData from '../../fakeData/listUserData';
import {
    PERCENT_CHART_AMOUNT_CHANGE,
    PERCENT_CHART_DEFAULT,
    PERCENT_CHART_MIN,
    PERCENT_CHART_MAX,
    WHEEL_AMOUNT_CHANGE,
    DEFAULT_NUMBER_CHILDREN_RENDER_ALL,
    DEFAULT_NUMBER_LAYER_SHOW_ALL,
    TYPE_DEPARTMENT,
} from '../../variables/orgChart';
import {
    updateNodeToMultiRoleNode,
    getDefaultName,
    getSiblingNode,
    getNewOrder,
    updateOrder,
    convertToListNodeDataShow,
    updateParent,
    getAllId,
    getParentNode,
    getDefaultDepartment,
    getNode
} from '../../utils';
import {
    INCREASE_PERCENT_CHART,
    DECREASE_PERCENT_CHART,
    SET_DEFAULT_PERCENT_CHART,
    ON_WHEEL_UPDATE_PERCENT_CHART,
    TOGGLE_COLLAPSE_NODE,
    ADD_CHILD_NODE,
    ADD_SIBLING_NODE,
    CHANGE_NODE_POSITION_TO_CHILDREN,
    CHANGE_NODE_POSITION_TO_SIBLING,
    CHANGE_ROOT_NODE,
    TOGGLE_NODE_DRAWER,
    TOGGLE_LIST_CHILD_DRAWER,
    ADD_USER_TO_NODE,
    SET_NODE_DRAGGING_ID,
    SET_RELATIVE_NODE,
} from '../actions/orgChart';

const getListNodeDataShow = (listNodeData, node, numberLayerShowAll, numberChildrenRenderAll) => {
    const newListNodeData = cloneDeep(listNodeData);
    updateNodeToMultiRoleNode(newListNodeData, null, numberLayerShowAll, numberChildrenRenderAll);
    return newListNodeData;
}

const listNodeDataShowDefault = getListNodeDataShow(listNodeDataTest, null, DEFAULT_NUMBER_LAYER_SHOW_ALL, DEFAULT_NUMBER_CHILDREN_RENDER_ALL);

const defaultState = {
    percentChart: PERCENT_CHART_DEFAULT,
    collapseChart: false,
    listNodeCollapse: {}, // hash map with true, false
    listNodeData: listNodeDataTest,
    listUser: listUserData,
    listUserAdded: listUserAddedDefault,
    listUserShow: [],
    listNodeDraggingId: [], // first id is the id of the node is dragging
    rootNode: undefined,
    nodeDrawerData: undefined, //node data of drawer will open
    numberChildrenRenderAll: DEFAULT_NUMBER_CHILDREN_RENDER_ALL,
    numberLayerShowAll: DEFAULT_NUMBER_LAYER_SHOW_ALL,
    listNodeDataShow: listNodeDataShowDefault,
    showListChildDrawer: undefined, // node data will show the list child drawer,
    relativeNode: undefined // node will draw a tree from it's parent to this node
};

const orgChartReducer = (state = defaultState, action) => {
    switch (action.type) {
        case INCREASE_PERCENT_CHART: {
            const { percentChart } = state;
            // for case 94% => 100%
            const newPercent = (Math.floor(percentChart / PERCENT_CHART_AMOUNT_CHANGE) + 1) * PERCENT_CHART_AMOUNT_CHANGE;
            const percentChartShow = newPercent > PERCENT_CHART_MAX ? PERCENT_CHART_MAX : newPercent;
            return {
                ...state,
                percentChart: percentChartShow
            }
        }
        case DECREASE_PERCENT_CHART: {
            const { percentChart } = state;
            // for case 94% => 75%
            const newPercent = (Math.ceil(percentChart / PERCENT_CHART_AMOUNT_CHANGE) - 1) * PERCENT_CHART_AMOUNT_CHANGE;
            const percentChartShow = newPercent < PERCENT_CHART_MIN ? PERCENT_CHART_MIN : newPercent;
            return {
                ...state,
                percentChart: percentChartShow
            }
        }
        case SET_DEFAULT_PERCENT_CHART: {
            return {
                ...state,
                percentChart: PERCENT_CHART_DEFAULT
            }
        }
        case ON_WHEEL_UPDATE_PERCENT_CHART: {
            const { deltaY } = action.payload;
            const { percentChart } = state;
            let newPercent = deltaY > 0 ? percentChart - WHEEL_AMOUNT_CHANGE : percentChart + WHEEL_AMOUNT_CHANGE;
            if (newPercent < PERCENT_CHART_MIN) {
                newPercent = PERCENT_CHART_MIN;
            }
            if (newPercent > PERCENT_CHART_MAX) {
                newPercent = PERCENT_CHART_MAX;
            }
            return {
                ...state,
                percentChart: newPercent
            }
        }
        case TOGGLE_COLLAPSE_NODE: {
            const { nodeId } = action.payload;
            return {
                ...state,
                listNodeCollapse: {
                    ...state.listNodeCollapse,
                    [nodeId]: !state.listNodeCollapse[nodeId]
                }
            }
        }
        case ADD_CHILD_NODE: {
            const { item, root, direction } = action.payload;
            const { nodeType, user, name, department = getDefaultDepartment() } = item;
            const randomId = Math.floor(Math.random() * 1000000).toString(); // hard code
            const nodeName = name ? name : getDefaultName(nodeType);
            const { listNodeData, listUserAdded, rootNode, numberLayerShowAll, numberChildrenRenderAll, relativeNode } = state;
            if (!root) {
                if (Object.keys(listNodeData.children).length === 1) {
                    // do nothing if had 1 root. only support 1 root
                    return state;
                }
                const newNodeData = {
                    id: randomId,
                    name: nodeName,
                    type: nodeType,
                    parentId: null,
                    parent: null,
                    children: {},
                    user,
                    order: 0
                };
                if (nodeType === TYPE_DEPARTMENT) {
                    newNodeData.department = department;
                }
                const newListUserAdded = [...listUserAdded];
                if (user) {
                    newListUserAdded.push(user);
                }
                const newListNodeData = {
                    ...state.listNodeData,
                    children: {
                        ...listNodeData.children,
                        [randomId]: newNodeData
                    }
                };
                const newListNodeDataShow = convertToListNodeDataShow({
                    listNodeData: newListNodeData,
                    rootNode,
                    numberLayerShowAll,
                    numberChildrenRenderAll,
                    relativeNode
                })
                return {
                    ...state,
                    listUserAdded: newListUserAdded,
                    listNodeData: newListNodeData,
                    listNodeDataShow: newListNodeDataShow
                }
            }
            const parentRoot = root.parent || [];
            const newListNodeData = cloneDeep(listNodeData);
            let tmp = newListNodeData.children;
            tmp = getSiblingNode(tmp, parentRoot);

            // find the order for new node
            const currentChildren = tmp[root.id].children;
            const newOrder = getNewOrder(currentChildren, direction);

            const newNodeData = {
                id: randomId,
                name: nodeName,
                type: nodeType,
                parentId: root.id,
                parent: [...parentRoot, root.id],
                children: {},
                user,
                order: newOrder
            };
            if (nodeType === TYPE_DEPARTMENT) {
                newNodeData.department = department;
            }
            currentChildren[randomId] = newNodeData;
            const newListUserAdded = [...listUserAdded];
            if (user) {
                newListUserAdded.push(user);
            }
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode,
                numberLayerShowAll,
                numberChildrenRenderAll,
                relativeNode
            })

            return {
                ...state,
                listUserAdded: newListUserAdded,
                listNodeData: newListNodeData,
                listNodeDataShow: newListNodeDataShow
            }
        }
        case ADD_USER_TO_NODE: {
            const { item, root } = action.payload;
            const parentRoot = root.parent || [];
            const rootId = root.id;
            const { user } = item;
            const { listNodeData, listUserAdded, rootNode, numberLayerShowAll, numberChildrenRenderAll, relativeNode } = state;
            const newListNodeData = cloneDeep(listNodeData);
            let tmp = newListNodeData.children;
            let newListUserAdded = [...listUserAdded];
            tmp = getSiblingNode(tmp, parentRoot);
            const prevUser = tmp[rootId].user;
            if (!prevUser) {
                // add user to node empty
                tmp[rootId].user = user;
                newListUserAdded.push(user);
            }
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode,
                numberLayerShowAll,
                numberChildrenRenderAll,
                relativeNode
            })
            return {
                ...state,
                listUserAdded: newListUserAdded,
                listNodeData: newListNodeData,
                listNodeDataShow: newListNodeDataShow
            }
        }
        case ADD_SIBLING_NODE: {
            const { item, root, direction } = action.payload;
            const { nodeType, user, name, department = getDefaultDepartment() } = item;
            const { listUserAdded, listNodeData, rootNode, numberLayerShowAll, numberChildrenRenderAll, relativeNode } = state;
            const newListUserAdded = [...listUserAdded];
            const newListNodeData = cloneDeep(listNodeData);
            if (user) {
                newListUserAdded.push(user);
            }
            const randomId = Math.floor(Math.random() * 1000000).toString(); // hard code
            const parentRoot = root.parent || [];
            const parentRootId = root.parentId;
            let tmp = newListNodeData.children;
            tmp = getSiblingNode(tmp, parentRoot);
            const newOrder = root.order;
            updateOrder(tmp, root, direction);
            const newNodeData = {
                id: randomId,
                name: name ? name : getDefaultName(nodeType),
                type: nodeType,
                parentId: parentRootId,
                parent: parentRoot,
                children: {},
                user,
                order: newOrder
            };
            if (nodeType === TYPE_DEPARTMENT) {
                newNodeData.department = department;
            }
            tmp[randomId] = newNodeData;
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode,
                numberLayerShowAll,
                numberChildrenRenderAll,
                relativeNode
            });
            return {
                ...state,
                listUserAdded: newListUserAdded,
                listNodeData: newListNodeData,
                listNodeDataShow: newListNodeDataShow
            }
        }
        case CHANGE_NODE_POSITION_TO_CHILDREN: {
            const { item, root, direction } = action.payload;
            const { listNodeData, numberChildrenRenderAll, numberLayerShowAll, rootNode, relativeNode } = state;
            const { nodeData } = item;
            const newNodeData = cloneDeep(nodeData);
            const newListNodeData = cloneDeep(listNodeData);
            const { id, parentId, type } = nodeData;
            const itemParent = newNodeData.parent || [];

            if (!root) {
                if (Object.keys(listNodeData.children).length === 1) {
                    // only support 1 root in chart
                    return state;
                }
                // change node position to root
                // do nothing if item is currently root an it's deparment
                if (parentId && type !== TYPE_DEPARTMENT) {
                    let tmp = newListNodeData.children;
                    tmp = getSiblingNode(tmp, itemParent);
                    // remove prev node
                    delete tmp[id];
                    // add node to new parent
                    newListNodeData.children[id] = newNodeData;
                    // upadate parent and parent Ids of node and children of this node
                    updateParent(newNodeData, parentId, null, null, true);

                }
            } else {
                // change position of node
                if (id === root.id || parentId === root.id) {
                    // do nothing when item is root or new parent is current parent
                    return state;
                }
                const rootParent = root.parent || [];
                const itemIsParentOfRoot = rootParent.includes(id);
                if (itemIsParentOfRoot) {
                    return state;
                }
                // only change position when item is not parent of root
                const rootId = root.id;
                let tmp = newListNodeData.children;
                tmp = getSiblingNode(tmp, itemParent);
                delete tmp[id];

                // add node to new parent
                tmp = newListNodeData.children;
                tmp = getSiblingNode(tmp, rootParent);
                tmp[root.id].children[id] = newNodeData;

                // upadate parent and parent Ids of node and children of this node
                updateParent(newNodeData, parentId, rootId, [...rootParent, rootId], true);
            }
            const newOrder = getNewOrder(root.children, direction);
            newNodeData.order = newOrder;
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode,
                numberLayerShowAll,
                numberChildrenRenderAll,
                relativeNode
            });
            return {
                ...state,
                listNodeData: newListNodeData,
                listNodeDataShow: newListNodeDataShow
            }
        }
        case CHANGE_NODE_POSITION_TO_SIBLING: {
            const { item, root, direction } = action.payload;
            const { listNodeData, rootNode, numberLayerShowAll, numberChildrenRenderAll, relativeNode } = state;
            const { nodeData } = item;
            const newNodeData = cloneDeep(nodeData);
            const newListNodeData = cloneDeep(listNodeData);
            const { id, parentId, type } = nodeData;
            const itemParent = newNodeData.parent || [];
            const { parentId: rootParentId, parent: rootParent = [], id: rootId } = root;
            if (!rootParentId) {
                // only support 1 root
                return state;
            }
            const itemIsParentOfRoot = rootParent.includes(id);
            if (itemIsParentOfRoot) {
                // only change position when item is not parent of root
                return state;
            }
            let tmp = newListNodeData.children;
            tmp = getSiblingNode(tmp, itemParent);

            // remove prev node
            delete tmp[id];

            tmp = newListNodeData.children;
            tmp = getSiblingNode(tmp, rootParent);

            updateOrder(tmp, root, direction);
            // add new node
            tmp[id] = newNodeData;
            tmp[id].order = root.order;
            updateParent(newNodeData, parentId, parentId, rootParent, true);
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode,
                numberLayerShowAll,
                numberChildrenRenderAll,
                relativeNode
            });
            return {
                ...state,
                listNodeData: newListNodeData,
                listNodeDataShow: newListNodeDataShow
            }
        }
        case CHANGE_ROOT_NODE: {
            const { node } = action.payload;
            const { listNodeData, numberChildrenRenderAll, numberLayerShowAll, relativeNode } = state;
            const newListNodeData = cloneDeep(listNodeData);
            if (!node || node.parentId === null) {
                const newListNodeDataShow = convertToListNodeDataShow({
                    listNodeData: newListNodeData,
                    rootNode: null,
                    numberChildrenRenderAll,
                    numberLayerShowAll,
                    relativeNode
                });
                return {
                    ...state,
                    listNodeDataShow: newListNodeDataShow,
                    rootNode: null
                }
            }
            let tmp = newListNodeData.children;
            const rootParent = node.parent || [];
            tmp = getSiblingNode(tmp, rootParent);
            const newRootNode = cloneDeep(tmp[node.id]);
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData: newListNodeData,
                rootNode: newRootNode,
                numberChildrenRenderAll,
                numberLayerShowAll,
                relativeNode: undefined
            });
            return {
                ...state,
                rootNode: newRootNode,
                listNodeDataShow: newListNodeDataShow,
                relativeNode: undefined
            }
        }
        case SET_NODE_DRAGGING_ID: {
            const { node } = action.payload;
            let newListNodeDraggingId = [];
            if (!isEmpty(node)) {
                newListNodeDraggingId = getAllId(node);
            }
            return {
                ...state,
                listNodeDraggingId: newListNodeDraggingId
            };
        }
        case TOGGLE_LIST_CHILD_DRAWER: {
            const { node, root } = action.payload;
            const { listNodeData } = state;
            if (!node && !root) {
                return {
                    ...state,
                    showListChildDrawer: undefined
                }
            }
            const newListNodeData = cloneDeep(listNodeData);
            if (isEmpty(root)) {
                const parent = node.parent || [];
                const parentNode = getParentNode(newListNodeData.children, parent);
                return {
                    ...state,
                    showListChildDrawer: parentNode
                }
            } else {
                const parent = root.parent || [];
                const newShowListChildDrawer = getNode(newListNodeData.children, parent, root.id);
                return {
                    ...state,
                    showListChildDrawer: newShowListChildDrawer
                }
            }
        }
        case TOGGLE_NODE_DRAWER: {
            const { drawerData } = action.payload;
            if (!drawerData) {
                return {
                    ...state,
                    nodeDrawerData: undefined
                }
            }
            return {
                ...state,
                nodeDrawerData: {
                    ...drawerData
                }
            }
        }
        case SET_RELATIVE_NODE: {
            const { node } = action.payload;
            const { listNodeData, rootNode, numberChildrenRenderAll, numberLayerShowAll } = state;
            const newListNodeDataShow = convertToListNodeDataShow({
                listNodeData,
                rootNode,
                numberChildrenRenderAll,
                numberLayerShowAll,
                relativeNode: node
            });
            return {
                ...state,
                relativeNode: node,
                listNodeDataShow: newListNodeDataShow,
            }
        }

        default:
            return state;
    }
};

export const namespace = 'orgChart';

export default orgChartReducer;