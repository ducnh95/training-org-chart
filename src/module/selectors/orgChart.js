import { get, isEmpty, cloneDeep } from 'lodash';
import { createSelector } from 'reselect';

import { namespace } from '../reducers/orgChart';
import { TYPE_DEPARTMENT, TYPE_ROLE } from '../../variables/orgChart';
import {
    getNode,
    countChildOfNode,
    countNumOfDepartment
} from '../../utils';

export const selectPercentChart = state => state[namespace].percentChart;
export const selectListNodeCollapse = state => state[namespace].listNodeCollapse;
export const selectNumberLayerShowAll = state => state[namespace].numberLayerShowAll;
export const selectNumberChildrenRenderAll = state => state[namespace].numberChildrenRenderAll;
export const selectListNodeData = state => state[namespace].listNodeData;
export const selectListNodeDataShow = state => state[namespace].listNodeDataShow;
export const selectListUser = state => state[namespace].listUser;
export const selectListUserAdded = state => state[namespace].listUserAdded;
export const selectRootNode = state => state[namespace].rootNode;
export const selectListNodeDraggingId = state => state[namespace].listNodeDraggingId;
export const selectShowListChildDrawer = state => state[namespace].showListChildDrawer;
export const selectNodeDrawerData = state => state[namespace].nodeDrawerData;
export const selectRelativeNode = state => state[namespace].relativeNode;

export const selectNumberChildOfNode = (nodeData) => createSelector(
    selectListNodeData,
    listNodeData => {
        if (isEmpty(nodeData)) {
            return 0;
        }
        const { type } = nodeData;
        if (type === TYPE_DEPARTMENT || type === TYPE_ROLE) {
            const nodeParent = nodeData.parent || [];
            const currentNode = getNode(listNodeData.children, nodeParent, nodeData.id);
            if (currentNode) {
                return countChildOfNode(currentNode);
            }
        }
        return 0;
    }
);

export const selectNumberDepartment = (nodeData) => createSelector(
    selectListNodeData,
    listNodeData => {
        if (isEmpty(nodeData)) {
            return 0;
        }
        const { type } = nodeData;
        if (type === TYPE_ROLE) {
            const nodeParent = nodeData.parent || [];
            const currentNode = getNode(listNodeData.children, nodeParent, nodeData.id);
            if (currentNode) {
                return countNumOfDepartment(currentNode);
            }
        }
        return 0;
    }
);

export const selectListChildDrawerData = (
    createSelector(
        selectListNodeData,
        selectShowListChildDrawer,
        (listNodeData, showListChildDrawer) => {
            if (isEmpty(showListChildDrawer)) {
                return;
            }
            const newListNodeData = cloneDeep(listNodeData);
            return getNode(newListNodeData.children, showListChildDrawer.parent, showListChildDrawer.id);
        }
    )
);
