import {
    DEFAULT_DIRECTION_ADD_NODE,
    DEFAULT_DIRECTION_CHANGE_NODE
} from '../../variables/orgChart';

export const INCREASE_PERCENT_CHART = 'orgChart/INCREASE_PERCENT_CHART';
export const DECREASE_PERCENT_CHART = 'orgChart/DECREASE_PERCENT_CHART';
export const SET_DEFAULT_PERCENT_CHART = 'orgChart/SET_DEFAULT_PERCENT_CHART';
export const TOGGLE_COLLAPSE_CHART = 'orgChart/TOGGLE_COLLAPSE_CHART';
export const ON_WHEEL_UPDATE_PERCENT_CHART = 'orgChart/ON_WHEEL_UPDATE_PERCENT_CHART';
export const TOGGLE_COLLAPSE_NODE = 'orgChart/TOGGLE_COLLAPSE_NODE';
export const ADD_CHILD_NODE_SAGA = 'orgChart/ADD_CHILD_NODE_SAGA';
export const ADD_CHILD_NODE = 'orgChart/ADD_CHILD_NODE';
export const ADD_SIBLING_NODE_SAGA = 'orgChart/ADD_SIBLING_NODE_SAGA'
export const ADD_SIBLING_NODE = 'orgChart/ADD_SIBLING_NODE';
export const ADD_USER_TO_NODE_SAGA = 'orgChart/ADD_USER_TO_NODE_SAGA';
export const ADD_USER_TO_NODE = 'orgChart/ADD_USER_TO_NODE';
export const CHANGE_NODE_POSITION_TO_CHILDREN_SAGA = 'orgChart/CHANGE_NODE_POSITION_TO_CHILDREN_SAGA';
export const CHANGE_NODE_POSITION_TO_CHILDREN = 'orgChart/CHANGE_NODE_POSITION_TO_CHILDREN';
export const CHANGE_NODE_POSITION_TO_SIBLING_SAGA = 'orgChart/CHANGE_NODE_POSITION_TO_SIBLING_SAGA';
export const CHANGE_NODE_POSITION_TO_SIBLING = 'orgChart/CHANGE_NODE_POSITION_TO_SIBLING';
export const CHANGE_ROOT_NODE = 'orgChart/CHANGE_ROOT_NODE';
export const TOGGLE_NODE_DRAWER = 'orgChart/TOGGLE_NODE_DRAWER';
export const SAVE_NODE_DRAWER_SAGA = 'orgChart/SAVE_NODE_DRAWER_SAGA';
export const SAVE_NODE_DRAWER = 'orgChart/SAVE_NODE_DRAWER';
export const TOGGLE_LIST_CHILD_DRAWER = 'orgChart/TOGGLE_LIST_CHILD_DRAWER';
export const SET_NODE_DRAGGING_ID = 'orgChart/SET_NODE_DRAGGING_ID';
export const SET_RELATIVE_NODE = 'orgChart/SET_RELATIVE_NODE';

export const increasePercentChart = () => ({
    type: INCREASE_PERCENT_CHART
});

export const decresePercentChart = () => ({
    type: DECREASE_PERCENT_CHART
});

export const setDefaultPercentChart = () => ({
    type: SET_DEFAULT_PERCENT_CHART
});

export const onWheelUpdatePercentChart = (deltaY) => ({
    type: ON_WHEEL_UPDATE_PERCENT_CHART,
    payload: {
        deltaY
    }
});

export const toggleCollapseNode = (nodeId) => ({
    type: TOGGLE_COLLAPSE_NODE,
    payload: {
        nodeId
    }
});

export const addChildNodeSaga = ({ item, root, direction, isChangeRelativeNode }) => ({
    type: ADD_CHILD_NODE_SAGA,
    payload: {
        item,
        root,
        direction,
        isChangeRelativeNode
    }
});

export const addChildNode = ({ item, root, direction = DEFAULT_DIRECTION_ADD_NODE }) => ({
    type: ADD_CHILD_NODE,
    payload: {
        item,
        root,
        direction
    }
});

export const addUserToNodeSaga = ({ item, root }) => ({
    type: ADD_USER_TO_NODE_SAGA,
    payload: {
        item,
        root
    }
});

export const addUserToNode = ({ item, root }) => ({
    type: ADD_USER_TO_NODE,
    payload: {
        item,
        root
    }
});

export const addSiblingNodeSaga = ({ item, root, direction }) => ({
    type: ADD_SIBLING_NODE_SAGA,
    payload: {
        item,
        root,
        direction
    }
});

export const addSiblingNode = ({ item, root, direction }) => ({
    type: ADD_SIBLING_NODE,
    payload: {
        item,
        root,
        direction
    }
});

export const changeNodePositionToChildrenSaga = ({ item, root, direction = DEFAULT_DIRECTION_CHANGE_NODE }) => ({
    type: CHANGE_NODE_POSITION_TO_CHILDREN_SAGA,
    payload: {
        item,
        root,
        direction
    }
});

export const changeNodePositionToChildren = ({ item, root, direction = DEFAULT_DIRECTION_CHANGE_NODE }) => ({
    type: CHANGE_NODE_POSITION_TO_CHILDREN,
    payload: {
        item,
        root,
        direction
    }
});

export const changeNodePositionToSiblingSaga = ({ item, root, direction = DEFAULT_DIRECTION_CHANGE_NODE }) => ({
    type: CHANGE_NODE_POSITION_TO_SIBLING_SAGA,
    payload: {
        item,
        root,
        direction
    }
});

export const changeNodePositionToSibling = ({ item, root, direction = DEFAULT_DIRECTION_CHANGE_NODE }) => ({
    type: CHANGE_NODE_POSITION_TO_SIBLING,
    payload: {
        item,
        root,
        direction
    }
});

export const changeRootNode = (node) => ({
    type: CHANGE_ROOT_NODE,
    payload: {
        node
    }
});

export const setNodeDraggingId = (node) => ({
    type: SET_NODE_DRAGGING_ID,
    payload: {
        node
    }
});

export const toggleListChildDrawer = ({ node, root }) => ({
    type: TOGGLE_LIST_CHILD_DRAWER,
    payload: {
        node,
        root
    }
});

export const toggleNodeDrawer = (drawerData) => ({
    type: TOGGLE_NODE_DRAWER,
    payload: {
        drawerData
    }
});

export const saveNodeDrawerSaga = (newNodeData) => ({
    type: SAVE_NODE_DRAWER_SAGA,
    payload: {
        newNodeData
    }
});

export const saveNodeDrawer = (newNodeData) => ({
    type: SAVE_NODE_DRAWER,
    payload: {
        newNodeData
    }
});

export const setRelativeNode = (node) => ({
    type: SET_RELATIVE_NODE,
    payload: {
        node
    }
});
