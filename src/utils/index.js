import { isEmpty, get, cloneDeep } from 'lodash';

import {
    TYPE_DEPARTMENT,
    TYPE_ROLE,
    TYPE_MULTI_USER,
    DEFAULT_ROLE_NAME,
    TYPE_ASSISTANT,
    DEFAULT_ASSISTANT_NAME,
    DEFAULT_DEPARTMENT_NAME,
    LEFT,
    RIGHT,
    DEFAULT_DEPARTMENT_DESCRIPTION
} from '../variables/orgChart';

export const countChildOfRole = (data) => {
    // get all direct child have type ROLE or ASSISTANT, if it's DEPARTMENT, count all direct DEPARTMENT child
    let sum = 0;
    Object.values(data.children).forEach(child => {
        if (child.type !== TYPE_DEPARTMENT) {
            sum++;
        } else {
            sum += Object.keys(child.children).length;
        }
    })

    return sum;
}

export const countChildOfDepartment = (data) => {
    // get all node have type ROLE or ASSISTANT
    let sum = 0;
    if (data.type !== TYPE_DEPARTMENT) {
        sum = 1;
    }
    if (!isEmpty(data.children)) {
        Object.values(data.children).forEach(item => {
            sum += countChildOfDepartment(item);
        });
    }
    return sum;
}

export const countChildOfNode = (nodeData) => {
    const { type } = nodeData;
    if (type === TYPE_ROLE) {
        return countChildOfRole(nodeData);
    }
    if (type === TYPE_DEPARTMENT) {
        return countChildOfDepartment(nodeData);
    }
    return 0;
}

export const getDepartmentId = () => Math.floor(Math.random() * 1000000).toString();


export const updateNodeToMultiRoleNode = (node, rootNode, numberLayerShowAll, numberChildrenRenderAll) => {
    const currentLayer = getLayerOfNode(node);
    const rootNodeLayer = getLayerOfNode(rootNode);
    const layerShowAll = numberLayerShowAll + rootNodeLayer - 1;
    const childrenLength = countChildOfNode(node);
    const { type } = node;
    if (currentLayer >= layerShowAll && childrenLength > numberChildrenRenderAll && type !== TYPE_DEPARTMENT) {
        // department doesn't count
        const MultiRoleNodeId = `${node.id}-multi-role`;
        const currentNodeParent = node.parent || [];
        return node.children = {
            [MultiRoleNodeId]: {
                id: MultiRoleNodeId,
                children: {},
                parentId: node.id,
                parent: [...currentNodeParent, node.id],
                type: TYPE_MULTI_USER,
                numOfChild: countChildOfNode(node)
            }
        }
    }
    Object.values(node.children).forEach(child => {
        updateNodeToMultiRoleNode(child, rootNode, numberLayerShowAll, numberChildrenRenderAll);
    })
};

export const getLayerOfNode = (node) => {
    if (!node) {
        return 1;
    }
    if (!node.id) {
        return 0;
    }
    return get(node, 'parent.length', 0) + 1;
};

export const getListNodeInLayer = (layer, data) => {
    if (layer === 1) {
        return [data.id];
    }
    let result = [];
    const children = get(data, 'children', {});
    if (Object.keys(children).length > 0) {
        Object.values(children).forEach(item => {
            result = [...result, ...getListNodeInLayer(layer - 1, item)];
        });
    }
    return result;
};

export const getDefaultName = (nodeType) => {
    switch (nodeType) {
        case TYPE_ROLE:
            return DEFAULT_ROLE_NAME;
        case TYPE_ASSISTANT:
            return DEFAULT_ASSISTANT_NAME;
        default:
            return DEFAULT_DEPARTMENT_NAME
    }
};


export const updateParent = (node, prevRootId, newRootId, newRootParent, isFirstCall) => {
    const prevParent = node.parent || [];
    const indexParentRemove = prevParent.indexOf(prevRootId);
    // set parentId and parent for the first node
    if (isFirstCall) {
        node.parentId = newRootId;
        node.parent = newRootParent
    } else {
        // only update parent for the child node
        const newParent = prevParent.slice(indexParentRemove + 1);
        if (newRootParent) {
            node.parent = [...newRootParent, ...newParent];
        } else {
            node.parent = newParent;
        }
    }
    const children = node.children;
    if (!isEmpty(children)) {
        Object.values(children).forEach(child => {
            updateParent(child, prevRootId, newRootId, newRootParent);
        })
    }
};

export const getAllId = (node) => {
    let result = [node.id];
    const children = node.children;
    if (isEmpty(children)) {
        return result;
    }
    Object.values(children).forEach(child => {
        result = [...result, ...getAllId(child)];
    });
    return result;
};

export const getNewOrder = (children, direction) => {
    const listOrder = Object.values(children).map(item => item.order);
    if (listOrder.length === 0) {
        return 0;
    }
    let newOrder = Math.max(...listOrder) + 1;
    if (direction === LEFT) {
        newOrder = Math.min(...listOrder) - 1;
    }
    return newOrder;
};


export const updateOrder = (children, root, direction) => {
    Object.values(children).forEach(child => {
        if (direction === RIGHT) {
            if (child.order <= root.order) {
                child.order = child.order - 1;
            } else {
                child.order = child.order + 1;
            }
        }
        if (direction === LEFT) {
            if (child.order >= root.order) {
                child.order = child.order + 1;
            } else {
                child.order = child.order - 1;
            }
        }
    })
};

export const getSiblingNode = (node, parent) => {
    let tmp;
    if (!parent) {
        return;
    }
    for (let i = 0; i < parent.length; i++) {
        tmp = node[parent[i]];
        if (!tmp) {
            break;
        }
        node = tmp.children;
    }
    return node;
};

export const getParentNode = (node, parent) => {
    let tmp;
    for (let i = 0; i < parent.length; i++) {
        tmp = node[parent[i]];
        if (!tmp) {
            break;
        }
        if (i === parent.length - 1) {
            node = tmp;
        } else {
            node = tmp.children;
        }
    }
    return node;
};

export const getNode = (listNodeData, parent, id) => {
    const listSiblingNode = getSiblingNode(listNodeData, parent);
    return listSiblingNode[id];
};

export const getNodeAndClone = (listNodeData, parent, id) => {
    return cloneDeep(getNode(listNodeData, parent, id));
};

export const convertToListNodeDataShow = ({
    listNodeData,
    rootNode,
    numberLayerShowAll,
    numberChildrenRenderAll,
    relativeNode
}) => {
    const newListNodeData = cloneDeep(listNodeData);
    if (isEmpty(relativeNode)) {
        if (!rootNode) {
            updateNodeToMultiRoleNode(newListNodeData, rootNode, numberLayerShowAll, numberChildrenRenderAll);
            return newListNodeData;
        }
        let tmp = newListNodeData.children;
        const rootParent = rootNode.parent || [];
        tmp = getSiblingNode(tmp, rootParent);
        const newRootNode = tmp[rootNode.id];
    
        updateNodeToMultiRoleNode(newListNodeData, newRootNode, numberLayerShowAll, numberChildrenRenderAll);
        return {
            id: null,
            children: {
                [newRootNode.id]: newRootNode
            }
        }
    }
    const newListNodeDataShow = {
        id: null,
        children : {}
    };
    let tmpDataShow = newListNodeDataShow;
    let tmpData = newListNodeData.children;
    const newRelativeNode = getNode(cloneDeep(listNodeData).children, relativeNode.parent, relativeNode.id);
    const { parent: relativeNodeParent } = newRelativeNode;
    relativeNodeParent.forEach(parentId => {
        tmpDataShow.children = {
            [parentId]: {
                ...tmpData[parentId],
                children: {}
            }
        };
        tmpDataShow = tmpDataShow.children[parentId];
        tmpData = tmpData[parentId].children;
    });
    tmpDataShow.children = {
        [relativeNode.id]: newRelativeNode
    }
    updateNodeToMultiRoleNode(newListNodeDataShow, null, numberLayerShowAll, numberChildrenRenderAll);

    return newListNodeDataShow;

};

export const getNumberChildren = (nodeData) => {
    if (isEmpty(nodeData)) {
        return 0;
    }
    const children = get(nodeData, 'children', {});
    return Object.keys(children).length;
};

export const getDefaultDepartment = () => ({
    name: DEFAULT_DEPARTMENT_NAME,
    id: getDepartmentId(),
    description: DEFAULT_DEPARTMENT_DESCRIPTION
});


export const countNumOfDepartment = (node) => {
    const children = get(node, 'children');
    if (!children) {
        return 0;
    }
    let count = 0;
    Object.values(children).forEach(child => {
        if (child.type === TYPE_DEPARTMENT) {
            count ++;
        }
    })
    return count;
}