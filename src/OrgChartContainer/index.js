import React, { useState, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    selectPercentChart,
    selectListNodeCollapse,
    selectNumberLayerShowAll,
    selectNumberChildrenRenderAll,
    selectListUserAdded,
    selectListUser,
    selectListNodeData,
    selectListNodeDataShow,
    selectRootNode,
    selectListNodeDraggingId,
    selectShowListChildDrawer,
    selectNodeDrawerData,
    selectRelativeNode
} from '../module/selectors';
import {
    increasePercentChart,
    decresePercentChart,
    setDefaultPercentChart,
    onWheelUpdatePercentChart,
    addChildNodeSaga,
    changeRootNode,
    toggleListChildDrawer,
    changeNodePositionToChildrenSaga,
    toggleNodeDrawer,
    setRelativeNode
} from '../module/actions';
import Sidebar from '../components/Sidebar';
import styles from './styles.module.scss';
import OrgChartToolbar from '../components/OrgChartToolbar';
import ReactOrgChart from '../components/ReactOrgChart';
import {
    TYPE_ROLE,
    TYPE_ASSISTANT,
    TYPE_DEPARTMENT,
    ACTION_ADD_ROLE,
    ACTION_ADD_ASSISTANT,
    ACTION_ADD_DEPARTMENT,
    PERCENT_CHART_MAX,
    ZOOMIN_LIMIT,
    ZOOMOUT_LIMIT
} from '../variables/orgChart';
import NodeDrawer from '../components/NodeDrawer';
import ListChildDrawer from '../components/ListChildDrawer';
import TestDrag from '../components/TestComponent/OrgChart/TestDrag';

const OrgChartContainer = ({
    percentChart,
    listNodeCollapse,
    increasePercentChart,
    decresePercentChart,
    setDefaultPercentChart,
    onWheelUpdatePercentChart,
    numberLayerShowAll,
    numberChildrenRenderAll,
    addChildNodeSaga,
    listUser,
    listUserAdded,
    listNodeData,
    listNodeDataShow,
    changeRootNode,
    rootNode,
    listNodeDraggingId,
    showListChildDrawer,
    toggleListChildDrawer,
    nodeDrawerData,
    toggleNodeDrawer,
    changeNodePositionToChildrenSaga,
    setRelativeNode,
    relativeNode
}) => {
    const [listUserShow, setListUserShow] = useState([]);

    useEffect(() => {
        // setList
        const listUserAddedId = listUserAdded.map(user => user.id);
        const newListUserShow = listUser.filter(user => !listUserAddedId.includes(user.id));
        setListUserShow(newListUserShow);
    }, [listUser, listUserAdded]);

    const onSaveNodeDrawer = useCallback((newNodeData) => {
        const { action, node, isChangeRelativeNode } = nodeDrawerData;
        switch (action) {
            case ACTION_ADD_ROLE:
                addChildNodeSaga({
                    item: {
                        nodeType: TYPE_ROLE,
                        ...newNodeData
                    },
                    root: node,
                    isChangeRelativeNode
                });
                break;
            case ACTION_ADD_ASSISTANT:
                addChildNodeSaga({
                    item: {
                        nodeType: TYPE_ASSISTANT,
                        ...newNodeData
                    },
                    root: node,
                    isChangeRelativeNode
                });
                break;
            case ACTION_ADD_DEPARTMENT:
                addChildNodeSaga({
                    item: {
                        nodeType: TYPE_DEPARTMENT,
                        ...newNodeData
                    },
                    root: node,
                    isChangeRelativeNode
                });
                break;
            default:
                break;
        }
    }, [nodeDrawerData, addChildNodeSaga]);

    const onWheel = useCallback((e) => {
        onWheelUpdatePercentChart(e.deltaY);
    }, [onWheelUpdatePercentChart]);

    return (
        <div className={styles['container']}>
            <Sidebar
                listUserShow={listUserShow}
            />
            <OrgChartToolbar
                onIncreasePercentChart={increasePercentChart}
                onDecreasePercentChart={decresePercentChart}
                onSetDefaultPercentChart={setDefaultPercentChart}
                percentChart={percentChart}
                rootNode={rootNode}
                changeRootNode={changeRootNode}
                setRelativeNode={setRelativeNode}
                relativeNode={relativeNode}
            />
            <ReactOrgChart
                percentChart={percentChart}
                onWheel={onWheel}
                percentMax={PERCENT_CHART_MAX}
                zoominLimit={ZOOMIN_LIMIT}
                zoomoutLimit={ZOOMOUT_LIMIT}
                listNodeCollapse={listNodeCollapse}
                datasource={listNodeDataShow}
                listNodeDraggingId={listNodeDraggingId}
                rootNode={rootNode}
                addChildNodeSaga={addChildNodeSaga}
                changeNodePositionToChildrenSaga={changeNodePositionToChildrenSaga}
            />
            <NodeDrawer
                nodeDrawerData={nodeDrawerData}
                toggleNodeDrawer={toggleNodeDrawer}
                listUser={listUser}
                onSaveNodeDrawer={onSaveNodeDrawer}
            />
            {!!showListChildDrawer && (
                <ListChildDrawer />
            )}

        </div>
    )
};

export default connect((state) => ({
    percentChart: selectPercentChart(state),
    listNodeCollapse: selectListNodeCollapse(state),
    numberLayerShowAll: selectNumberLayerShowAll(state),
    numberChildrenRenderAll: selectNumberChildrenRenderAll(state),
    listUser: selectListUser(state),
    listUserAdded: selectListUserAdded(state),
    listNodeData: selectListNodeData(state),
    listNodeDataShow: selectListNodeDataShow(state),
    rootNode: selectRootNode(state),
    listNodeDraggingId: selectListNodeDraggingId(state),
    showListChildDrawer: selectShowListChildDrawer(state),
    nodeDrawerData: selectNodeDrawerData(state),
    relativeNode: selectRelativeNode(state)
}), {
    increasePercentChart,
    decresePercentChart,
    setDefaultPercentChart,
    onWheelUpdatePercentChart,
    addChildNodeSaga,
    changeRootNode,
    toggleListChildDrawer,
    changeNodePositionToChildrenSaga,
    toggleNodeDrawer,
    setRelativeNode
})(OrgChartContainer);
