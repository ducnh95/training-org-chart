import React from 'react';
import ReactDOM from 'react-dom';
import Backend from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd'
import 'antd/dist/antd.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux'
import MouseBackEnd from 'react-dnd-mouse-backend'

import './index.scss';
import * as serviceWorker from './serviceWorker';
import OrgChartContainer from './OrgChartContainer';
import store from './module/store';

ReactDOM.render(
  <Provider store={store}>
    <DndProvider backend={Backend}>
      <OrgChartContainer />
    </DndProvider>
  </Provider>,
  document.getElementById('root')
);

export * from './components';

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
